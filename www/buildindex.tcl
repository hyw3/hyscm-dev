#!/usr/bin/env tclsh
#
# Run this TCL script to generate a WIKI page that contains a
# permuted index of the various documentation files.
#
#    tclsh buildindex.tcl
#

set doclist {
  aboutcgi.wiki {How CGI Works In HySCM}
  aboutdownload.wiki {How The Download Page Works}
  adding_code.wiki {Adding New Features To HySCM}
  adding_code.wiki {Hacking HySCM}
  admin-v-setup.md {The Differences Between the Setup and Admin User Capabilities}
  alerts.md {Email Alerts And Notifications}
  antibot.wiki {Defense against Spiders and Bots}
  backoffice.md {The "Backoffice" mechanism of HySCM}
  blame.wiki {The Annotate/Blame Algorithm Of HySCM}
  blockchain.md {HySCM As Blockchain}
  branching.wiki {Branching, Forking, Merging, and Tagging}
  bugtheory.wiki {Bug Tracking In HySCM}
  build.wiki {Compiling and Installing HySCM}
  changes.wiki {HySCM Changelog}
  checkin_names.wiki {Check-in And Version Names}
  checkin.wiki {Check-in Checklist}
  childprojects.wiki {Child Projects}
  copyright-release.html {Contributor License Agreement}
  concepts.wiki {HySCM Core Concepts}
  contribute.wiki {Contributing Code or Documentation To The HySCM Project}
  customgraph.md {Theming: Customizing the Timeline Graph}
  customskin.md {Theming: Customizing The Appearance of Web Pages}
  customskin.md {Custom Skins}
  custom_ticket.wiki {Customizing The Ticket System}
  delta_encoder_algorithm.wiki {HySCM Delta Encoding Algorithm}
  delta_format.wiki {HySCM Delta Format}
  embeddeddoc.wiki {Embedded Project Documentation}
  encryptedrepos.wiki {How To Use Encrypted Repositories}
  env-opts.md {Environment Variables and Global Options}
  event.wiki {Events}
  faq.wiki {Frequently Asked Questions}
  fileformat.wiki {HySCM File Format}
  fiveminutes.wiki {Up and Running in 5 Minutes as a Single User}
  forum.wiki {HySCM Forums}
  foss-cklist.wiki {Checklist For Successful Open-Source Projects}
  hyscm-from-msvc.wiki {Integrating HySCM in the Microsoft Express 2010 IDE}
  hyscm-v-git.wiki {HySCM Versus Git}
  globs.md {File Name Glob Patterns}
  grep.md {HySCM grep vs POSIX grep}
  hacker-howto.wiki {Hacker How-To}
  hashpolicy.wiki {Hash Policy: Choosing Between SHA1 and SHA3-256}
  /help {Lists of Commands and Webpages}
  hints.wiki {HySCM Tips And Usage Hints}
  index.wiki {Home Page}
  inout.wiki {Import And Export To And From Git}
  image-format-vs-repo-size.md {Image Format vs HySCM Repo Size}
  makefile.wiki {The HySCM Build Process}
  mirrortogithub.md {How To Mirror A HySCM Repository On GitHub}
  /md_rules {Markdown Formatting Rules}
  newrepo.wiki {How To Create A New HySCM Repository}
  password.wiki {Password Management And Authentication}
  pop.wiki {Principles Of Operation}
  private.wiki {Creating, Syncing, and Deleting Private Branches}
  qandc.wiki {Questions And Criticisms}
  quickstart.wiki {HySCM Quick Start Guide}
  quotes.wiki
      {Quotes: What People Are Saying About HySCM, Git, and DVCSes in General}
  ../test/release-checklist.wiki {Pre-Release Testing Checklist}
  reviews.wiki {Reviews}
  selfcheck.wiki {HySCM Repository Integrity Self Checks}
  selfhost.wiki {HySCM Self Hosting Repositories}
  server.wiki {How To Configure A HySCM Server}
  settings.wiki {HySCM Settings}
  /sitemap {Site Map}
  shunning.wiki {Shunning: Deleting Content From HySCM}
  stats.wiki {Performance Statistics}
  style.wiki {Source Code Style Guidelines}
  ssl.wiki {Using SSL with HySCM}
  sync.wiki {The HySCM Sync Protocol}
  tech_overview.wiki {A Technical Overview Of The Design And Implementation
                      Of HySCM}
  tech_overview.wiki {SQLite Databases Used By HySCM}
  th1.md {The TH1 Scripting Language}
  tickets.wiki {The HySCM Ticket System}
  theory1.wiki {Thoughts On The Design Of The HySCM DVCS}
  tls-nginx.md {Proxying HySCM via HTTPS with nginx}
  unvers.wiki {Unversioned Files}
  webpage-ex.md {Webpage Examples}
  webui.wiki {The HySCM Web Interface}
  whyusehyscm.wiki {Why You Should Use HySCM}
  whyusehyscm.wiki {Benefits Of Version Control}
  wikitheory.wiki {Wiki In HySCM}
  /wiki_rules {Wiki Formatting Rules}
}

set permindex {}
set stopwords {
   a about against and are as by for hyscm from in of on or should the to use
   used with
}
foreach {file title} $doclist {
  set n [llength $title]
  regsub -all {\s+} $title { } title
  lappend permindex [list $title $file 1]
  for {set i 0} {$i<$n-1} {incr i} {
    set prefix [lrange $title 0 $i]
    set suffix [lrange $title [expr {$i+1}] end]
    set firstword [string tolower [lindex $suffix 0]]
    if {[lsearch $stopwords $firstword]<0} {
      lappend permindex [list "$suffix &mdash; $prefix" $file 0]
    }
  }
}
set permindex [lsort -dict -index 0 $permindex]
set out [open permutedindex.html w]
fconfigure $out -encoding utf-8 -translation lf
puts $out \
"<div class='hyscm-doc' data-title='Index Of HySCM Documentation'>"
puts $out {
<center>
<form action='$ROOT/docsrch' method='GET'>
<input type="text" name="s" size="40" autofocus>
<input type="submit" value="Search Docs">
</form>
</center>
<h2>Primary Documents:</h2>
<ul>
<li> <a href='quickstart.wiki'>Quick-start Guide</a>
<li> <a href='faq.wiki'>FAQ</a>
<li> <a href='build.wiki'>Compiling and installing HySCM</a>
<li> <a href='../COPYRIGHT.txt'>License</a>
<li> <a href='$ROOT/help'>List of commands, web-pages, and settings</a>
<li> <a href='hacker-howto.wiki'>Hacker How-To</a>
</ul>
<a name="pindex"></a>
<h2>Permuted Index:</h2>
<ul>}
foreach entry $permindex {
  foreach {title file bold} $entry break
  if {$bold} {set title <b>$title</b>}
  if {[string match /* $file]} {set file ../../..$file}
  puts $out "<li><a href=\"$file\">$title</a></li>"
}
puts $out "</ul></div>"
