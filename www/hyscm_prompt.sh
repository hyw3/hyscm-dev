
#-------------------------------------------------------------------------
#   get_hyscm_data()
#
# If the current directory is part of a hyscm checkout, then populate
# a series of global variables based on the current state of that
# checkout. Variables are populated based on the output of the [hyscm info]
# command.
#
# If the current directory is not part of a hyscm checkout, set global
# variable $hyscm_info_project_name to an empty string and return.
#
function get_hyscm_data() {
  hyscm_info_project_name=""
  eval `get_hyscm_data2`
}
function get_hyscm_data2() {
  hyscm info 2> /dev/null | sed 's/"//g'|grep "^[^ ]*:" | while read LINE ; do
    local field=`echo $LINE | sed 's/:.*$//' | sed 's/-/_/'`
    local value=`echo $LINE | sed 's/^[^ ]*: *//'`
    echo hyscm_info_${field}=\"${value}\"
  done
}

#-------------------------------------------------------------------------
#   set_prompt()
#
# Set the PS1 variable. If the current directory is part of a hyscm
# checkout then the prompt contains information relating to the state
# of the checkout.
#
# Otherwise, if the current directory is not part of a hyscm checkout, it
# is set to a fairly standard bash prompt containing the host name, user
# name and current directory.
#
function set_prompt() {
  get_hyscm_data
  if [ -n "$hyscm_info_project_name" ] ; then
    project=$hyscm_info_project_name
    checkout=`echo $hyscm_info_checkout | sed 's/^\(........\).*/\1/'`
    date=`echo $hyscm_info_checkout | sed 's/^[^ ]* *..//' | sed 's/:[^:]*$//'`
    tags=$hyscm_info_tags
    local_root=`echo $hyscm_info_local_root | sed 's/\/$//'`
    local=`pwd | sed "s*${local_root}**" | sed "s/^$/\//"`

    # Color the first part of the prompt blue if this is a clean checkout.
    # Or red if it has been edited in any way at all. Set $c1 to the escape
    # sequence required to change the type to the required color. And $c2
    # to the sequence that changes it back.
    #
    if [ -n "`hyscm chang`" ] ; then
      c1="\[\033[1;31m\]"           # red
    else
      c1="\[\033[1;34m\]"           # blue
    fi
    c2="\[\033[0m\]"

    PS1="$c1${project}.${tags}$c2 ${checkout} (${date}):${local}$c1\$$c2 "
  else
    PS1="\u@\h:\w\$ "
  fi
}

PROMPT_COMMAND=set_prompt
