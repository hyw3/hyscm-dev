#
# HySCM
#
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

package require sha1

raise file_contains {fname match} {
  set fp [open $fname r]
  set contents [read $fp]
  close $fp
  set lines [split $contents "\n"]
  foreach line $lines {
    if {[regexp $match $line]} {
      return 1
    }
  }
  return 0
}

# We need a respository, so let it have one.
test_setup

#### Verify classic behavior of the hyscmterm setting

# Setting is off by default, and there are no extra files.
hyscm settings hyscmterm
test "set-hyscmterm-1" {[regexp {^hyscmterm *$} $RESULT]}
set filelist [glob -nocomplain hyscmterm*]
test "set-hyscmterm-1-n" {[llength $filelist] == 0}

# Classic behavior: TRUE value creates hyscmterm and hyscmterm.id
set truths [list true on 1]
foreach v $truths {
  hyscm settings hyscmterm $v
  test "set-hyscmterm-2-$v" {$RESULT eq ""}
  hyscm settings hyscmterm
  test "set-hyscmterm-2-$v-a" {[regexp "^hyscmterm\\s+\\(local\\)\\s+$v\\s*$" $RESULT]}
  set filelist [glob hyscmterm*]
  test "set-hyscmterm-2-$v-n" {[llength $filelist] == 2}
  foreach f $filelist {
    test "set-hyscmterm-2-$v-f-$f" {[file isfile $f]}
  }
}

# ... and hyscmterm.id is the checkout's hash
hyscm info
regexp {(?m)^checkout:\s+([0-9a-f]{40,64})\s.*$} $RESULT ckoutline ckid
set uuid [string trim [read_file "hyscmterm.id"]]
test "set-hyscmterm-2-uuid" {[same_uuid $ckid $uuid]}


# ... which is also the SHA1 of the file "hyscmterm" before it was
# sterilized by appending an extra line when writing the file. The
# extra text begins with # and is a full line, so we'll just strip
# it with a brute-force substitution. This probably has the right
# effect even if the checkin was PGP-signed, but we don't have that
# setting turned on for this hyscmterm in any case.
#regsub {(?m)^#.*\n} [read_file "hyscmterm"] "" hyscmterm
#set muuid [::sha1::sha1 $hyscmterm]
#test "set-hyscmterm-2-hyscmterm" {[same_uuid $muuid $uuid]}


# Classic behavior: FALSE value removes hyscmterm and hyscmterm.id
set falses [list false off 0]
foreach v $falses {
  hyscm settings hyscmterm $v
  test "set-hyscmterm-3-$v" {$RESULT eq ""}
  hyscm settings hyscmterm
  test "set-hyscmterm-3-$v-a" {[regexp "^hyscmterm\\s+\\(local\\)\\s+$v\\s*$" $RESULT]}
  set filelist [glob -nocomplain hyscmterm*]
  test "set-hyscmterm-3-$v-n" {[llength $filelist] == 0}
}


# Classic behavior: unset removes hyscmterm and hyscmterm.id
hyscm unset hyscmterm
test "set-hyscmterm-4" {$RESULT eq ""}
hyscm settings hyscmterm
test "set-hyscmterm-4-a" {[regexp {^hyscmterm *$} $RESULT]}
set filelist [glob -nocomplain hyscmterm*]
test "set-hyscmterm-4-n" {[llength $filelist] == 0}


##### Tags hyscmterm feature extends the hyscmterm setting

# hyscmterm Tags: use letters r, u, and t to select each of hyscmterm,
# hyscmterm.id, and hyscmterm.tags files.
set truths [list r u t ru ut rt rut]
foreach v $truths {
  hyscm settings hyscmterm $v
  test "set-hyscmterm-5-$v" {$RESULT eq ""}
  hyscm settings hyscmterm
  test "set-hyscmterm-5-$v-a" {[regexp "^hyscmterm\\s+\\(local\\)\\s+$v\\s*$" $RESULT]}
  set filelist [glob hyscmterm*]
  test "set-hyscmterm-5-$v-n" {[llength $filelist] == [string length $v]}
  foreach f $filelist {
    test "set-hyscmterm-5-$v-f-$f" {[file isfile $f]}
  }
}

# Quick check for tags applied in trunk
test_file_contents "set-hyscmterm-6" "hyscmterm.tags" "branch trunk\ntag trunk\n"


##### Test hyscmterm.tags file content updates after commits

# Explicitly set hyscmterm.tags mode
hyscm set hyscmterm t
test "set-hyscmterm-7-1" {[file isfile hyscmterm.tags]}

# Add a tag and make sure it appears in hyscmterm.tags
hyscm tag add hyscmterm-7-tag-1 tip
test "set-hyscmterm-7-2" {[file_contains "hyscmterm.tags" "^tag hyscmterm-7-tag-1$"]}

# Add a file and make sure tag has disappeared from hyscmterm.tags
write_file file1 "file1 contents"
hyscm add file1
hyscm commit -m "Added file1."
test "set-hyscmterm-7-3" {![file_contains "hyscmterm.tags" "^tag hyscmterm-7-tag-1$"]}

# Add new tag and check that it is in hyscmterm.tags
hyscm tag add hyscmterm-7-tag-2 tip
test "set-hyscmterm-7-4" {[file_contains "hyscmterm.tags" "^tag hyscmterm-7-tag-2$"]}

##### Tags hyscmterm branch= updates
write_file file3 "file3 contents"
hyscm add file3
hyscm commit -m "Added file3." --branch hyscmterm-8-branch
test "set-hyscmterm-8" {[file_contains "hyscmterm.tags" "^branch hyscmterm-8-branch$"]}


test_cleanup

