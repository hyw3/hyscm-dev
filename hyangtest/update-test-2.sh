#!/bin/sh
#
export HYSCM=$1
rm -rf aaa bbb update-test-2.hyscm

# Create a test repository
$HYSCM new update-test-2.hyscm

# In checkout aaa, add file one.hyang.
mkdir aaa
cd aaa
$HYSCM open ../update-test-2.hyscm
echo one >one.hyang
$HYSCM add one.hyang
$HYSCM commit -m add-one --tag add-one

# Create checkout bbb.
mkdir ../bbb
cd ../bbb
$HYSCM open ../update-test-2.hyscm

# Back in aaa, make changes to one.hyang.  Add file two.hyang.
cd ../aaa
echo change >>one.hyang
echo two >two.hyang
$HYSCM add two.hyang
$HYSCM commit -m 'chng one and add two' --tag add-two

# In bbb, remove one.hyang, then update.
cd ../bbb
$HYSCM rm one.hyang
$HYSCM changes
echo '============================================================================'
$HYSCM update
echo '======== Previous should show "ADD two.hyang" and conflict on one.hyang ===='
$HYSCM changes
echo '======== The previous should show "DELETE one.hyang" ======================='
$HYSCM commit --test -m 'check-in'
echo '======== Only file two.hyang is checked in ================================='

