#!/bin/sh
#
export HYSCM=$1
rm -rf aaa bbb update-test-1.hyscm

# Create a test repository
$HYSCM new update-test-1.hyscm

# In checkout aaa, add file one.hyang
mkdir aaa
cd aaa
$HYSCM open ../update-test-1.hyscm
echo one >one.hyang
$HYSCM add one.hyang
$HYSCM commit -m add-one --tag add-one

# Open checkout bbb.
mkdir ../bbb
cd ../bbb
$HYSCM open ../update-test-1.hyscm

# Back in aaa, add file two.hyang
cd ../aaa
echo two >two.hyang
$HYSCM add two.hyang
$HYSCM commit -m add-two --tag add-two

# In bbb, delete file one.hyang.  Then update the change from aaa that
# adds file two.  Verify that one.hyang says deleted.
cd ../bbb
$HYSCM rm one.hyang
$HYSCM changes
echo '=========================================================================='
$HYSCM update
echo '======== The previous should show "ADD two.hyang" ========================'
$HYSCM changes
echo '======== The previous should show "DELETE one.hyang" ====================='
$HYSCM commit --test -m check-in
echo '======== Only file two.hyang is checked in ==============================='

