/*
** HySCM
**
** Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
** Copyright (C) 2019 Hyang Language Foundation, Jakarta
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#define _LARGE_FILE       1
#ifndef _FILE_OFFSET_BITS
#  define _FILE_OFFSET_BITS 64
#endif
#define _LARGEFILE_SOURCE 1

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#if defined(_WIN32) && !defined(_WIN64) && !defined(_MSC_VER) && !defined(_USE_32BIT_TIME_T)
#  define _USE_32BIT_TIME_T
#endif

#ifdef HAVE_HYSCMSETUP_H
#include "hyscmsetup.h"
#endif

#ifndef HYSCM_HARDENED_SHA1
# define HYSCM_HARDENED_SHA1 1
#endif

#ifndef _RC_COMPILE_

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

#endif

#if defined( __MINGW32__) ||  defined(__DMC__) || defined(_MSC_VER) || defined(__POCC__)
#  if defined(__DMC__)  || defined(_MSC_VER) || defined(__POCC__)
     typedef int socklen_t;
#  endif
#  ifndef _WIN32
#    define _WIN32
#  endif
#else
# include <sys/types.h>
# include <signal.h>
# include <pwd.h>
#endif

#if !defined(COMPILER_STRINGIFY)
#  define COMPILER_STRINGIFY(x)  COMPILER_STRINGIFY1(x)
#  define COMPILER_STRINGIFY1(x) #x
#endif

#if !defined(COMPILER_NAME)
#  if defined(__DMC__)
#    if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#      define COMPILER_NAME "dmc-" COMPILER_VERSION
#    else
#      define COMPILER_NAME "dmc"
#    endif
#  elif defined(__POCC__)
#    if defined(_M_X64)
#      if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#        define COMPILER_NAME "pellesc64-" COMPILER_VERSION
#      else
#        define COMPILER_NAME "pellesc64"
#      endif
#    else
#      if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#        define COMPILER_NAME "pellesc32-" COMPILER_VERSION
#      else
#        define COMPILER_NAME "pellesc32"
#      endif
#    endif
#  elif defined(__clang__)
#    if !defined(COMPILER_VERSION)
#      if defined(__clang_version__)
#        define COMPILER_VERSION __clang_version__
#      endif
#    endif
#    if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#      define COMPILER_NAME "clang-" COMPILER_VERSION
#    else
#      define COMPILER_NAME "clang"
#    endif
#  elif defined(_MSC_VER)
#    if !defined(COMPILER_VERSION)
#      define COMPILER_VERSION COMPILER_STRINGIFY(_MSC_VER)
#    endif
#    if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#      define COMPILER_NAME "msc-" COMPILER_VERSION
#    else
#      define COMPILER_NAME "msc"
#    endif
#  elif defined(__MINGW32__)
#    if !defined(COMPILER_VERSION)
#      if defined(__MINGW_VERSION)
#        if defined(__GNUC__)
#          if defined(__VERSION__)
#            define COMPILER_VERSION COMPILER_STRINGIFY(__MINGW_VERSION) "-gcc-" __VERSION__
#          else
#            define COMPILER_VERSION COMPILER_STRINGIFY(__MINGW_VERSION) "-gcc"
#          endif
#        else
#          define COMPILER_VERSION COMPILER_STRINGIFY(__MINGW_VERSION)
#        endif
#      elif defined(__MINGW32_VERSION)
#        if defined(__GNUC__)
#          if defined(__VERSION__)
#            define COMPILER_VERSION COMPILER_STRINGIFY(__MINGW32_VERSION) "-gcc-" __VERSION__
#          else
#            define COMPILER_VERSION COMPILER_STRINGIFY(__MINGW32_VERSION) "-gcc"
#          endif
#        else
#          define COMPILER_VERSION COMPILER_STRINGIFY(__MINGW32_VERSION)
#        endif
#      endif
#    endif
#    if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#      define COMPILER_NAME "mingw32-" COMPILER_VERSION
#    else
#      define COMPILER_NAME "mingw32"
#    endif
#  elif defined(__GNUC__)
#    if !defined(COMPILER_VERSION)
#      if defined(__VERSION__)
#        define COMPILER_VERSION __VERSION__
#      endif
#    endif
#    if defined(COMPILER_VERSION) && !defined(NO_COMPILER_VERSION)
#      define COMPILER_NAME "gcc-" COMPILER_VERSION
#    else
#      define COMPILER_NAME "gcc"
#    endif
#  elif defined(_WIN32)
#    define COMPILER_NAME "win32"
#  else
#    define COMPILER_NAME "unknown"
#  endif
#endif

#if !defined(_RC_COMPILE_) && !defined(SQLITE_AMALGAMATION)

#if defined(_MSC_VER) && _MSC_VER<1600
   typedef __int32 int32_t;
   typedef unsigned __int32 uint32_t;
   typedef __int64 int64_t;
   typedef unsigned __int64 uint64_t;
#else
#  include <stdint.h>
#endif

#if USE_SEE && !defined(SQLITE_HAS_CODEC)
#  define SQLITE_HAS_CODEC
#endif
#include "sqlite3.h"

#if HAVE_GETPASSPHRASE
  #define getpass getpassphrase
#endif

typedef sqlite3_int64 i64;
typedef sqlite3_uint64 u64;

typedef unsigned char u8;
typedef signed char i8;

#define MN_CKIN_MSG   100
#define MX_CKIN_MSG   300

#if defined(__PTRDIFF_TYPE__)
# define HYSCM_INT_TO_PTR(X)  ((void*)(__PTRDIFF_TYPE__)(X))
# define HYSCM_PTR_TO_INT(X)  ((int)(__PTRDIFF_TYPE__)(X))
#elif !defined(__GNUC__)
# define HYSCM_INT_TO_PTR(X)  ((void*)&((char*)0)[X])
# define HYSCM_PTR_TO_INT(X)  ((int)(((char*)X)-(char*)0))
#else
# define HYSCM_INT_TO_PTR(X)  ((void*)(X))
# define HYSCM_PTR_TO_INT(X)  ((int)(X))
#endif

#if defined(__GNUC__) || defined(__clang__)
# define NORETURN __attribute__((__noreturn__))
#elif defined(_MSC_VER) && (_MSC_VER >= 1310)
# define NORETURN __declspec(noreturn)
#else
# define NORETURN
#endif

#define count(X) (sizeof(X)/sizeof(X[0]))

#if !defined(HAVE_PLEDGE)
# define hyscm_pledge(A)
#endif


#endif /* _RC_COMPILE_ */
