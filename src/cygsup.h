/*
** HySCM
**
** Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
** Copyright (C) 2019 Hyang Language Foundation, Jakarta
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#if defined(__CYGWIN__) && !defined(CYGSUP_H)
#define CYGSUP_H

/*
*******************************************************************************
** Include any Cygwin-specific headers here.                                 **
*******************************************************************************
*/

#include <wchar.h>
#include <sys/cygwin.h>

/*
*******************************************************************************
** Define any Cygwin-specific preprocessor macros here.  All macros defined in
** this section should be wrapped with #ifndef, in order to allow them to be
** externally overridden.
*******************************************************************************
*/

#ifndef CP_UTF8
#  define CP_UTF8            65001
#endif

#ifndef WINBASEAPI
#  define WINBASEAPI         __declspec(dllimport)
#endif

#ifndef WINADVAPI
#  define WINADVAPI          __declspec(dllimport)
#endif

#ifndef SHSTDAPI
#  define SHSTDAPI           __declspec(dllimport)
#endif

#ifndef STDAPI
#  define STDAPI             __stdcall
#endif

#ifndef WINAPI
#  define WINAPI             __stdcall
#endif

/*
*******************************************************************************
** Declare any Cygwin-specific Win32 or other APIs here.  Functions declared in
** this section should use the built-in ANSI C types in order to make sure this
** header file continues to work as a self-contained unit.
**
** On Cygwin64, "long" is 64-bit but in Win64 it's 32-bit.  That's why in the
** signatures below "long" should not be used.  They now use "int" instead.
*******************************************************************************
*/

WINADVAPI extern WINAPI int RegOpenKeyExW(
    void *,          /* HKEY */
    const wchar_t *, /* LPCWSTR */
    unsigned int,    /* DWORD */
    unsigned int,    /* REGSAM */
    void *           /* PHKEY */
    );

WINADVAPI extern WINAPI int RegQueryValueExW(
    void *,          /* HKEY */
    const wchar_t *, /* LPCWSTR */
    unsigned int *,  /* LPDWORD */
    unsigned int *,  /* LPDWORD */
    unsigned char *, /* LPBYTE */
    unsigned int *   /* LPDWORD */
    );

SHSTDAPI extern STDAPI void *ShellExecuteW(
    void *,          /* HWND */
    const wchar_t *, /* LPCWSTR */
    const wchar_t *, /* LPCWSTR */
    const wchar_t *, /* LPCWSTR */
    const wchar_t *, /* LPCWSTR */
    int              /* INT */
    );

WINBASEAPI extern WINAPI int WideCharToMultiByte(
    unsigned int,    /* UINT */
    unsigned int,    /* DWORD */
    const wchar_t *, /* LPCWSTR */
    int,             /* int */
    char *,          /* LPSTR */
    int,             /* int */
    const char *,    /* LPCSTR */
    int *            /* LPBOOL */
    );

WINBASEAPI extern WINAPI int MultiByteToWideChar(
    unsigned int,    /* UINT */
    unsigned int,    /* DWORD */
    const char *,    /* LPCSTR */
    int,             /* int */
    wchar_t *,       /* LPWSTR */
    int              /* int */
    );

#endif /* defined(__CYGWIN__) && !defined(CYGSUP_H) */
