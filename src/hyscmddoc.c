/*
** HySCM
**
** Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
** Copyright (C) 2019 Hyang Language Foundation, Jakarta
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/
#include "config.h"
#include "hyscmddoc.h"
#include <assert.h>

/*
** Mengkestrak konten yang berisi mimetype.
**
** Jika konten berisi teks saja, keluarkan output NULL.
**
** Untuk jenis konten gambar, keluarkan output mimetype
** dengan nama seperti "image/gif" atau "image/jpeg".
**
** Untuk jenis berkas biner lain, hasilnya "unknown/unknown".
*/
const char *mimetype_from_content(Blob *pBlob){
  int i;
  int n;
  const unsigned char *x;

  /* Tabel mimetype berdasarkan prefik konten berkas
  */
  static const struct {
    const char *zPrefix;       /* Prefik berkas */
    int size;                  /* Panjang prefik */
    const char *zMimetype;     /* Jenis mimetype yang sesuai */
  } aMime[] = {
    { "GIF87a",                  6, "image/gif"  },
    { "GIF89a",                  6, "image/gif"  },
    { "\211PNG\r\n\032\n",       8, "image/png"  },
    { "\377\332\377",            3, "image/jpeg" },
    { "\377\330\377",            3, "image/jpeg" },
  };

  if( !looks_like_binary(pBlob) ) {
    return 0;   /* Plain text */
  }
  x = (const unsigned char*)blob_buffer(pBlob);
  n = blob_size(pBlob);
  for(i=0; i<count(aMime); i++){
    if( n>=aMime[i].size && memcmp(x, aMime[i].zPrefix, aMime[i].size)==0 ){
      return aMime[i].zMimetype;
    }
  }
  return "unknown/unknown";
}

/* Tabel mimetype berdasarkan nama sufik berkas.
** Sufik harus diurutkan sehingga memungkinkan pencarian
** biner untuk menemukan mime-type
*/
static const struct {
  const char *zSuffix;       /* Sufik berkas */
  int size;                  /* Panjang sufik */
  const char *zMimetype;     /* Mimetype yang sesuai */
} aMime[] = {
  { "ai",         2, "application/postscript"            },
  { "aif",        3, "audio/x-aiff"                      },
  { "aifc",       4, "audio/x-aiff"                      },
  { "aiff",       4, "audio/x-aiff"                      },
  { "arj",        3, "application/x-arj-compressed"      },
  { "asc",        3, "text/plain"                        },
  { "asf",        3, "video/x-ms-asf"                    },
  { "asx",        3, "video/x-ms-asx"                    },
  { "au",         2, "audio/ulaw"                        },
  { "avi",        3, "video/x-msvideo"                   },
  { "bat",        3, "application/x-msdos-program"       },
  { "bcpio",      5, "application/x-bcpio"               },
  { "bin",        3, "application/octet-stream"          },
  { "bmp",        3, "image/bmp"                         },
  { "bz2",        3, "application/x-bzip2"               },
  { "bzip",       4, "application/x-bzip"                },
  { "c",          1, "text/plain"                        },
  { "cc",         2, "text/plain"                        },
  { "ccad",       4, "application/clariscad"             },
  { "cdf",        3, "application/x-netcdf"              },
  { "class",      5, "application/octet-stream"          },
  { "cod",        3, "application/vnd.rim.cod"           },
  { "com",        3, "application/x-msdos-program"       },
  { "cpio",       4, "application/x-cpio"                },
  { "cpt",        3, "application/mac-compactpro"        },
  { "cs",         2, "text/plain"                        },
  { "csh",        3, "application/x-csh"                 },
  { "css",        3, "text/css"                          },
  { "csv",        3, "text/csv"                          },
  { "dcr",        3, "application/x-director"            },
  { "deb",        3, "application/x-debian-package"      },
  { "dib",        3, "image/bmp"                         },
  { "dir",        3, "application/x-director"            },
  { "dl",         2, "video/dl"                          },
  { "dms",        3, "application/octet-stream"          },
  { "doc",        3, "application/msword"                },
  { "docx",       4, "application/vnd.openxmlformats-"
                     "officedocument.wordprocessingml.document"},
  { "dot",        3, "application/msword"                },
  { "dotx",       4, "application/vnd.openxmlformats-"
                     "officedocument.wordprocessingml.template"},
  { "drw",        3, "application/drafting"              },
  { "dvi",        3, "application/x-dvi"                 },
  { "dwg",        3, "application/acad"                  },
  { "dxf",        3, "application/dxf"                   },
  { "dxr",        3, "application/x-director"            },
  { "eps",        3, "application/postscript"            },
  { "etx",        3, "text/x-setext"                     },
  { "exe",        3, "application/octet-stream"          },
  { "ez",         2, "application/andrew-inset"          },
  { "f",          1, "text/plain"                        },
  { "f90",        3, "text/plain"                        },
  { "fli",        3, "video/fli"                         },
  { "flv",        3, "video/flv"                         },
  { "gif",        3, "image/gif"                         },
  { "gl",         2, "video/gl"                          },
  { "gtar",       4, "application/x-gtar"                },
  { "gz",         2, "application/x-gzip"                },
  { "h",          1, "text/plain"                        },
  { "hdf",        3, "application/x-hdf"                 },
  { "hh",         2, "text/plain"                        },
  { "hqx",        3, "application/mac-binhex40"          },
  { "htm",        3, "text/html"                         },
  { "html",       4, "text/html"                         },
  { "ice",        3, "x-conference/x-cooltalk"           },
  { "ico",        3, "image/vnd.microsoft.icon"          },
  { "ief",        3, "image/ief"                         },
  { "iges",       4, "model/iges"                        },
  { "igs",        3, "model/iges"                        },
  { "ips",        3, "application/x-ipscript"            },
  { "ipx",        3, "application/x-ipix"                },
  { "jad",        3, "text/vnd.sun.j2me.app-descriptor"  },
  { "jar",        3, "application/java-archive"          },
  { "jpe",        3, "image/jpeg"                        },
  { "jpeg",       4, "image/jpeg"                        },
  { "jpg",        3, "image/jpeg"                        },
  { "js",         2, "application/x-javascript"          },
  { "kar",        3, "audio/midi"                        },
  { "latex",      5, "application/x-latex"               },
  { "lha",        3, "application/octet-stream"          },
  { "lsp",        3, "application/x-lisp"                },
  { "lzh",        3, "application/octet-stream"          },
  { "m",          1, "text/plain"                        },
  { "m3u",        3, "audio/x-mpegurl"                   },
  { "man",        3, "text/plain"                        },
  { "markdown",   8, "text/x-markdown"                   },
  { "md",         2, "text/x-markdown"                   },
  { "me",         2, "application/x-troff-me"            },
  { "mesh",       4, "model/mesh"                        },
  { "mid",        3, "audio/midi"                        },
  { "midi",       4, "audio/midi"                        },
  { "mif",        3, "application/x-mif"                 },
  { "mime",       4, "www/mime"                          },
  { "mkd",        3, "text/x-markdown"                   },
  { "mov",        3, "video/quicktime"                   },
  { "movie",      5, "video/x-sgi-movie"                 },
  { "mp2",        3, "audio/mpeg"                        },
  { "mp3",        3, "audio/mpeg"                        },
  { "mp4",        3, "video/mp4"                         },
  { "mpe",        3, "video/mpeg"                        },
  { "mpeg",       4, "video/mpeg"                        },
  { "mpg",        3, "video/mpeg"                        },
  { "mpga",       4, "audio/mpeg"                        },
  { "ms",         2, "application/x-troff-ms"            },
  { "msh",        3, "model/mesh"                        },
  { "n",          1, "text/plain"                        },
  { "nc",         2, "application/x-netcdf"              },
  { "oda",        3, "application/oda"                   },
  { "odp",        3, "application/vnd.oasis.opendocument.presentation" },
  { "ods",        3, "application/vnd.oasis.opendocument.spreadsheet" },
  { "odt",        3, "application/vnd.oasis.opendocument.text" },
  { "ogg",        3, "application/ogg"                   },
  { "ogm",        3, "application/ogg"                   },
  { "pbm",        3, "image/x-portable-bitmap"           },
  { "pdb",        3, "chemical/x-pdb"                    },
  { "pdf",        3, "application/pdf"                   },
  { "pgm",        3, "image/x-portable-graymap"          },
  { "pgn",        3, "application/x-chess-pgn"           },
  { "pgp",        3, "application/pgp"                   },
  { "pl",         2, "application/x-perl"                },
  { "pm",         2, "application/x-perl"                },
  { "png",        3, "image/png"                         },
  { "pnm",        3, "image/x-portable-anymap"           },
  { "pot",        3, "application/mspowerpoint"          },
  { "potx",       4, "application/vnd.openxmlformats-"
                     "officedocument.presentationml.template"},
  { "ppm",        3, "image/x-portable-pixmap"           },
  { "pps",        3, "application/mspowerpoint"          },
  { "ppsx",       4, "application/vnd.openxmlformats-"
                     "officedocument.presentationml.slideshow"},
  { "ppt",        3, "application/mspowerpoint"          },
  { "pptx",       4, "application/vnd.openxmlformats-"
                     "officedocument.presentationml.presentation"},
  { "ppz",        3, "application/mspowerpoint"          },
  { "pre",        3, "application/x-freelance"           },
  { "prt",        3, "application/pro_eng"               },
  { "ps",         2, "application/postscript"            },
  { "qt",         2, "video/quicktime"                   },
  { "ra",         2, "audio/x-realaudio"                 },
  { "ram",        3, "audio/x-pn-realaudio"              },
  { "rar",        3, "application/x-rar-compressed"      },
  { "ras",        3, "image/cmu-raster"                  },
  { "rgb",        3, "image/x-rgb"                       },
  { "rm",         2, "audio/x-pn-realaudio"              },
  { "roff",       4, "application/x-troff"               },
  { "rpm",        3, "audio/x-pn-realaudio-plugin"       },
  { "rtf",        3, "text/rtf"                          },
  { "rtx",        3, "text/richtext"                     },
  { "scm",        3, "application/x-lotusscreencam"      },
  { "set",        3, "application/set"                   },
  { "sgm",        3, "text/sgml"                         },
  { "sgml",       4, "text/sgml"                         },
  { "sh",         2, "application/x-sh"                  },
  { "shar",       4, "application/x-shar"                },
  { "silo",       4, "model/mesh"                        },
  { "sit",        3, "application/x-stuffit"             },
  { "skd",        3, "application/x-koan"                },
  { "skm",        3, "application/x-koan"                },
  { "skp",        3, "application/x-koan"                },
  { "skt",        3, "application/x-koan"                },
  { "smi",        3, "application/smil"                  },
  { "smil",       4, "application/smil"                  },
  { "snd",        3, "audio/basic"                       },
  { "sol",        3, "application/solids"                },
  { "spl",        3, "application/x-futuresplash"        },
  { "src",        3, "application/x-wais-source"         },
  { "step",       4, "application/STEP"                  },
  { "stl",        3, "application/SLA"                   },
  { "stp",        3, "application/STEP"                  },
  { "sv4cpio",    7, "application/x-sv4cpio"             },
  { "sv4crc",     6, "application/x-sv4crc"              },
  { "svg",        3, "image/svg+xml"                     },
  { "swf",        3, "application/x-shockwave-flash"     },
  { "t",          1, "application/x-troff"               },
  { "tar",        3, "application/x-tar"                 },
  { "tcl",        3, "application/x-tcl"                 },
  { "tex",        3, "application/x-tex"                 },
  { "texi",       4, "application/x-texinfo"             },
  { "texinfo",    7, "application/x-texinfo"             },
  { "tgz",        3, "application/x-tar-gz"              },
  { "th1",        3, "application/x-th1"                 },
  { "tif",        3, "image/tiff"                        },
  { "tiff",       4, "image/tiff"                        },
  { "tr",         2, "application/x-troff"               },
  { "tsi",        3, "audio/TSP-audio"                   },
  { "tsp",        3, "application/dsptype"               },
  { "tsv",        3, "text/tab-separated-values"         },
  { "txt",        3, "text/plain"                        },
  { "unv",        3, "application/i-deas"                },
  { "ustar",      5, "application/x-ustar"               },
  { "vb",         2, "text/plain"                        },
  { "vcd",        3, "application/x-cdlink"              },
  { "vda",        3, "application/vda"                   },
  { "viv",        3, "video/vnd.vivo"                    },
  { "vivo",       4, "video/vnd.vivo"                    },
  { "vrml",       4, "model/vrml"                        },
  { "wav",        3, "audio/x-wav"                       },
  { "wax",        3, "audio/x-ms-wax"                    },
  { "wiki",       4, "text/x-hyscm-wiki"                },
  { "wma",        3, "audio/x-ms-wma"                    },
  { "wmv",        3, "video/x-ms-wmv"                    },
  { "wmx",        3, "video/x-ms-wmx"                    },
  { "wrl",        3, "model/vrml"                        },
  { "wvx",        3, "video/x-ms-wvx"                    },
  { "xbm",        3, "image/x-xbitmap"                   },
  { "xlc",        3, "application/vnd.ms-excel"          },
  { "xll",        3, "application/vnd.ms-excel"          },
  { "xlm",        3, "application/vnd.ms-excel"          },
  { "xls",        3, "application/vnd.ms-excel"          },
  { "xlsx",       4, "application/vnd.openxmlformats-"
                     "officedocument.spreadsheetml.sheet"},
  { "xlw",        3, "application/vnd.ms-excel"          },
  { "xml",        3, "text/xml"                          },
  { "xpm",        3, "image/x-xpixmap"                   },
  { "xwd",        3, "image/x-xwindowdump"               },
  { "xyz",        3, "chemical/x-pdb"                    },
  { "zip",        3, "application/zip"                   },
};

/*
** Memverifikasi bahwa semua entri ada di tabel aMime[] dengan pengurutan.
** Batalkan dengan fatal error jika tidak terurut.
*/
static void mimetype_verify(void){
  int i;
  for(i=1; i<count(aMime); i++){
    if( hyscm_strcmp(aMime[i-1].zSuffix,aMime[i].zSuffix)>=0 ){
      hyscm_panic("mimetypes out of sequence: %s before %s",
                   aMime[i-1].zSuffix, aMime[i].zSuffix);
    }
  }
}

/*
** Menebak mime-type dokumen berdasarkan namanya.
*/
const char *mimetype_from_name(const char *zName){
  const char *z;
  int i;
  int first, last;
  int len;
  char zSuffix[20];


#ifdef HYSCM_DEBUG
  /* Pengujian kode untuk memastikan tabel di atas ada dalam
  ** urutan yang benar.
  */
  if( hyscm_strcmp(zName, "mimetype-test")==0 ){
    mimetype_verify();
    return "ok";
  }
#endif

  z = zName;
  for(i=0; zName[i]; i++){
    if( zName[i]=='.' ) z = &zName[i+1];
  }
  len = strlen(z);
  if( len<sizeof(zSuffix)-1 ){
    sqlite3_snprintf(sizeof(zSuffix), zSuffix, "%s", z);
    for(i=0; zSuffix[i]; i++) zSuffix[i] = hyscm_tolower(zSuffix[i]);
    first = 0;
    last = count(aMime) - 1;
    while( first<=last ){
      int c;
      i = (first+last)/2;
      c = hyscm_strcmp(zSuffix, aMime[i].zSuffix);
      if( c==0 ) return aMime[i].zMimetype;
      if( c<0 ){
        last = i-1;
      }else{
        first = i+1;
      }
    }
  }
  return "application/x-hyscm-artifact";
}

/*
** COMMAND: test-mimetype
*/
void mimetype_test_cmd(void){
  int i;
  mimetype_verify();
  for(i=2; i<g.argc; i++){
    hyscm_print("%-20s -> %s\n", g.argv[i], mimetype_from_name(g.argv[i]));
  }
}

/*
** WEBPAGE: mimetype_list
*/
void mimetype_list_page(void){
  int i;
  mimetype_verify();
  style_header("Mimetype List");
  @ <p>The HySCM <a href="%R/help?cmd=/doc">/doc</a> page uses filename
  @ suffixes and the following table to guess at the appropriate mimetype
  @ for each document.</p>
  @ <table class='sortable mimetypetable' border=1 cellpadding=0 \
  @ data-column-types='tt' data-init-sort='1'>
  @ <thead>
  @ <tr><th>Suffix<th>Mimetype
  @ </thead>
  @ <tbody>
  for(i=0; i<count(aMime); i++){
    @ <tr><td>%h(aMime[i].zSuffix)<td>%h(aMime[i].zMimetype)</tr>
  }
  @ </tbody></table>
  style_table_sorter();
  style_footer();
}

/*
** Check to see if the file in the pContent blob is "embedded HTML".  Return
** true if it is, and fill pTitle with the document title.
**
** An "embedded HTML" file is HTML that lacks a header and a footer.  The
** standard HySCM header is prepended and the standard HySCM footer is
** appended.  Otherwise, the file is displayed without change.
**
** Embedded HTML must be contained in a <div class='hyscm-doc'> element.
** If that <div> also contains a data-title attribute, then the
** value of that attribute is extracted into pTitle and becomes the title
** of the document.
*/
int doc_is_embedded_html(Blob *pContent, Blob *pTitle){
  const char *zIn = blob_str(pContent);
  const char *zAttr;
  const char *zValue;
  int nAttr, nValue;
  int seenClass = 0;
  int seenTitle = 0;

  while( hyscm_isspace(zIn[0]) ) zIn++;
  if( hyscm_strnicmp(zIn,"<div",4)!=0 ) return 0;
  zIn += 4;
  while( zIn[0] ){
    if( hyscm_isspace(zIn[0]) ) zIn++;
    if( zIn[0]=='>' ) return 0;
    zAttr = zIn;
    while( hyscm_isalnum(zIn[0]) || zIn[0]=='-' ) zIn++;
    nAttr = (int)(zIn - zAttr);
    while( hyscm_isspace(zIn[0]) ) zIn++;
    if( zIn[0]!='=' ) continue;
    zIn++;
    while( hyscm_isspace(zIn[0]) ) zIn++;
    if( zIn[0]=='"' || zIn[0]=='\'' ){
      char cDelim = zIn[0];
      zIn++;
      zValue = zIn;
      while( zIn[0] && zIn[0]!=cDelim ) zIn++;
      if( zIn[0]==0 ) return 0;
      nValue = (int)(zIn - zValue);
      zIn++;
    }else{
      zValue = zIn;
      while( zIn[0]!=0 && zIn[0]!='>' && zIn[0]!='/'
            && !hyscm_isspace(zIn[0]) ) zIn++;
      if( zIn[0]==0 ) return 0;
      nValue = (int)(zIn - zValue);
    }
    if( nAttr==5 && hyscm_strnicmp(zAttr,"class",5)==0 ){
      if( nValue!=10 || hyscm_strnicmp(zValue,"hyscm-doc",10)!=0 ) return 0;
      seenClass = 1;
      if( seenTitle ) return 1;
    }
    if( nAttr==10 && hyscm_strnicmp(zAttr,"data-title",10)==0 ){
      blob_append(pTitle, zValue, nValue);
      seenTitle = 1;
      if( seenClass ) return 1;
    }
  }
  return seenClass;
}

/*
** Look for a file named zName in the check-in with RID=vid.  Load the content
** of that file into pContent and return the RID for the file.  Or return 0
** if the file is not found or could not be loaded.
*/
int doc_load_content(int vid, const char *zName, Blob *pContent){
  int writable = db_is_writeable("repository");
  int rid;   /* The RID of the file being loaded */
  if( writable ){
    db_end_transaction(0);
    db_begin_write();
  }
  if( !db_table_exists("repository", "vcache") || !writable ){
    db_multi_exec(
      "CREATE %s TABLE IF NOT EXISTS vcache(\n"
      "  vid INTEGER,         -- check-in ID\n"
      "  fname TEXT,          -- filename\n"
      "  rid INTEGER,         -- artifact ID\n"
      "  PRIMARY KEY(vid,fname)\n"
      ") WITHOUT ROWID", writable ? "" : "TEMPORARY"
    );
  }
  if( !db_exists("SELECT 1 FROM vcache WHERE vid=%d", vid) ){
    db_multi_exec(
      "DELETE FROM vcache;\n"
      "CREATE VIRTUAL TABLE IF NOT EXISTS temp.foci USING files_of_checkin;\n"
      "INSERT INTO vcache(vid,fname,rid)"
      "  SELECT checkinID, filename, blob.rid FROM foci, blob"
      "   WHERE blob.uuid=foci.uuid"
      "     AND foci.checkinID=%d;",
      vid
    );
  }
  rid = db_int(0, "SELECT rid FROM vcache"
                  " WHERE vid=%d AND fname=%Q", vid, zName);
  if( rid && content_get(rid, pContent)==0 ){
    rid = 0;
  }
  return rid;
}

/*
** Transfer content to the output.  During the transfer, when text of
** the following form is seen:
**
**       href="$ROOT/
**       action="$ROOT/
**
** Convert $ROOT to the root URI of the repository.  Allow ' in place of "
** and any case for href or action.
*/
void convert_href_and_output(Blob *pIn){
  int i, base;
  int n = blob_size(pIn);
  char *z = blob_buffer(pIn);
  for(base=0, i=7; i<n; i++){
    if( z[i]=='$'
     && strncmp(&z[i],"$ROOT/", 6)==0
     && (z[i-1]=='\'' || z[i-1]=='"')
     && i-base>=9
     && (hyscm_strnicmp(&z[i-7]," href=", 6)==0 ||
           hyscm_strnicmp(&z[i-9]," action=", 8)==0)
    ){
      blob_append(cgi_output_blob(), &z[base], i-base);
      blob_appendf(cgi_output_blob(), "%R");
      base = i+5;
    }
  }
  blob_append(cgi_output_blob(), &z[base], i-base);
}

/*
** WEBPAGE: uv
** WEBPAGE: doc
*/
void doc_page(void){
  const char *zName = 0;            /* Argument to the /doc page */
  const char *zOrigName = "?";      /* Original document name */
  const char *zMime;                /* Document MIME type */
  char *zCheckin = "tip";           /* The check-in holding the document */
  char *zPathSuffix = "";           /* Text to append to g.zPath */
  int vid = 0;                      /* Artifact of check-in */
  int rid = 0;                      /* Artifact of file */
  int i;                            /* Loop counter */
  Blob filebody;                    /* Content of the documentation file */
  Blob title;                       /* Document title */
  int nMiss = (-1);                 /* Failed attempts to find the document */
  int isUV = g.zPath[0]=='u';       /* True for /uv.  False for /doc */
  const char *zDfltTitle;
  static const char *const azSuffix[] = {
     "index.html", "index.wiki", "index.md"
#ifdef HYSCM_ENABLE_TH1_DOCS
      , "index.th1"
#endif
  };

  login_check_credentials();
  if( !g.perm.Read ){ login_needed(g.anon.Read); return; }
  blob_init(&title, 0, 0);
  zDfltTitle = isUV ? "" : "Documentation";
  db_begin_transaction();
  while( rid==0 && (++nMiss)<=count(azSuffix) ){
    zName = P("name");
    if( isUV ){
      if( zName==0 ) zName = "index.wiki";
      i = 0;
    }else{
      if( zName==0 || zName[0]==0 ) zName = "tip/index.wiki";
      for(i=0; zName[i] && zName[i]!='/'; i++){}
      zCheckin = mprintf("%.*s", i, zName);
      if( hyscm_strcmp(zCheckin,"ckout")==0 && g.localOpen==0 ){
        zCheckin = "tip";
      }
    }
    if( nMiss==count(azSuffix) ){
      zName = "404.md";
    }else if( zName[i]==0 ){
      assert( nMiss>=0 && nMiss<count(azSuffix) );
      zName = azSuffix[nMiss];
    }else if( !isUV ){
      zName += i;
    }
    while( zName[0]=='/' ){ zName++; }
    if( isUV ){
      zPathSuffix = hyscm_strdup(zName);
    }else{
      zPathSuffix = mprintf("%s/%s", zCheckin, zName);
    }
    if( nMiss==0 ) zOrigName = zName;
    if( !file_is_simple_pathname(zName, 1) ){
      if( sqlite3_strglob("*/", zName)==0 ){
        assert( nMiss>=0 && nMiss<count(azSuffix) );
        zName = mprintf("%s%s", zName, azSuffix[nMiss]);
        if( !file_is_simple_pathname(zName, 1) ){
          goto doc_not_found;
        }
      }else{
        goto doc_not_found;
      }
    }
    if( isUV ){
      if( db_table_exists("repository","unversioned") ){
        Stmt q;
        db_prepare(&q, "SELECT hash, mtime FROM unversioned"
                       " WHERE name=%Q", zName);
        if( db_step(&q)==SQLITE_ROW ){
          etag_check(ETAG_HASH, db_column_text(&q,0));
          etag_last_modified(db_column_int64(&q,1));
        }
        db_finalize(&q);
        if( unversioned_content(zName, &filebody)==0 ){
          rid = 1;
          zDfltTitle = zName;
        }
      }
    }else if( hyscm_strcmp(zCheckin,"ckout")==0 ){
      /* Membaca checkout lokal */
      char *zFullpath;
      db_must_be_within_tree();
      zFullpath = mprintf("%s/%s", g.zLocalRoot, zName);
      if( file_isfile(zFullpath, RepoFILE)
       && blob_read_from_file(&filebody, zFullpath, RepoFILE)>0 ){
        rid = 1;
      }
      hyscm_free(zFullpath);
    }else{
      vid = symbolic_name_to_rid(zCheckin, "ci");
      rid = vid>0 ? doc_load_content(vid, zName, &filebody) : 0;
    }
  }
  g.zPath = mprintf("%s/%s", g.zPath, zPathSuffix);
  if( rid==0 ) goto doc_not_found;
  blob_to_utf8_no_bom(&filebody, 0);

  /* Berkas sekarang terkandung dalam blob.
  ** Kemudian kirimkan ke antarmuka web.
  */
  zMime = nMiss==0 ? P("mimetype") : 0;
  if( zMime==0 ){
    zMime = mimetype_from_name(zName);
  }
  Th_Store("doc_name", zName);
  if( vid ){
    Th_Store("doc_version", db_text(0, "SELECT '[' || substr(uuid,1,10) || ']'"
                                       "  FROM blob WHERE rid=%d", vid));
    Th_Store("doc_date", db_text(0, "SELECT datetime(mtime) FROM event"
                                    " WHERE objid=%d AND type='ci'", vid));
  }
  if( hyscm_strcmp(zMime, "text/x-hyscm-wiki")==0 ){
    Blob tail;
    style_adunit_config(ADUNIT_RIGHT_OK);
    if( wiki_find_title(&filebody, &title, &tail) ){
      style_header("%s", blob_str(&title));
      wiki_convert(&tail, 0, WIKI_BUTTONS);
    }else{
      style_header("%s", zDfltTitle);
      wiki_convert(&filebody, 0, WIKI_BUTTONS);
    }
    style_footer();
  }else if( hyscm_strcmp(zMime, "text/x-markdown")==0 ){
    Blob tail = BLOB_INITIALIZER;
    markdown_to_html(&filebody, &title, &tail);
    if( blob_size(&title)>0 ){
      style_header("%s", blob_str(&title));
    }else{
      style_header("%s", nMiss>=count(azSuffix)?
                        "Not Found" : zDfltTitle);
    }
    convert_href_and_output(&tail);
    style_footer();
  }else if( hyscm_strcmp(zMime, "text/plain")==0 ){
    style_header("%s", zDfltTitle);
    @ <blockquote><pre>
    @ %h(blob_str(&filebody))
    @ </pre></blockquote>
    style_footer();
  }else if( hyscm_strcmp(zMime, "text/html")==0
            && doc_is_embedded_html(&filebody, &title) ){
    if( blob_size(&title)==0 ) blob_append(&title,zName,-1);
    style_header("%s", blob_str(&title));
    convert_href_and_output(&filebody);
    style_footer();
#ifdef HYSCM_ENABLE_TH1_DOCS
  }else if( Th_AreDocsEnabled() &&
            hyscm_strcmp(zMime, "application/x-th1")==0 ){
    int raw = P("raw")!=0;
    if( !raw ){
      Blob tail;
      blob_zero(&tail);
      if( wiki_find_title(&filebody, &title, &tail) ){
        style_header("%s", blob_str(&title));
        Th_Render(blob_str(&tail));
        blob_reset(&tail);
      }else{
        style_header("%h", zName);
        Th_Render(blob_str(&filebody));
      }
    }else{
      Th_Render(blob_str(&filebody));
    }
    if( !raw ){
      style_footer();
    }
#endif
  }else{
    cgi_set_content_type(zMime);
    cgi_set_content(&filebody);
  }
  if( nMiss>=count(azSuffix) ) cgi_set_status(404, "Not Found");
  db_end_transaction(0);
  return;

  
doc_not_found:
  db_end_transaction(0);
  if( isUV && P("name")==0 ){
    uvlist_page();
    return;
  }
  cgi_set_status(404, "Not Found");
  style_header("Not Found");
  @ <p>Document %h(zOrigName) not found
  if( hyscm_strcmp(zCheckin,"ckout")!=0 ){
    @ in %z(href("%R/tree?ci=%T",zCheckin))%h(zCheckin)</a>
  }
  style_footer();
  return;
}

/*
** Logo bawaan warna putih, GIF 16x16 piksel bisa disesuaikan kemudian.
*/
static const unsigned char aLogo[] = {
    71,  73,  70,  56,  57,  97,  16,   0,  16,   0,
   240,   0,   0, 255, 255, 255,   0,   0,   0,  33,
   254,   4, 119, 105, 115, 104,   0,  44,   0,   0,
     0,   0,  16,   0,  16,   0,   0,   2,  14, 132,
   143, 169, 203, 237,  15, 163, 156, 180, 218, 139,
   179,  62,   5,   0,  59,
};

/*
** WEBPAGE: logo
*/
void logo_page(void){
  Blob logo;
  char *zMime;

  etag_check(ETAG_CONFIG, 0);
  zMime = db_get("logo-mimetype", "image/gif");
  blob_zero(&logo);
  db_blob(&logo, "SELECT value FROM config WHERE name='logo-image'");
  if( blob_size(&logo)==0 ){
    blob_init(&logo, (char*)aLogo, sizeof(aLogo));
  }
  cgi_set_content_type(zMime);
  cgi_set_content(&logo);
}

/*
** Gambar default untuk latar belakang: berkas GIF 16x16 warna putih
*/
static const unsigned char aBackground[] = {
    71,  73,  70,  56,  57,  97,  16,   0,  16,   0,
   240,   0,   0, 255, 255, 255,   0,   0,   0,  33,
   254,   4, 119, 105, 115, 104,   0,  44,   0,   0,
     0,   0,  16,   0,  16,   0,   0,   2,  14, 132,
   143, 169, 203, 237,  15, 163, 156, 180, 218, 139,
   179,  62,   5,   0,  59,
};


/*
** WEBPAGE: background
*/
void background_page(void){
  Blob bgimg;
  char *zMime;

  etag_check(ETAG_CONFIG, 0);
  zMime = db_get("background-mimetype", "image/gif");
  blob_zero(&bgimg);
  db_blob(&bgimg, "SELECT value FROM config WHERE name='background-image'");
  if( blob_size(&bgimg)==0 ){
    blob_init(&bgimg, (char*)aBackground, sizeof(aBackground));
  }
  cgi_set_content_type(zMime);
  cgi_set_content(&bgimg);
}

/*
** WEBPAGE: docsrch
*/
void doc_search_page(void){
  login_check_credentials();
  style_header("Document Search");
  search_screen(SRCH_DOC, 0);
  style_footer();
}

