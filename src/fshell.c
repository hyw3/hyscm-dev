/*
** HySCM
**
** Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
** Copyright (C) 2019 Hyang Language Foundation, Jakarta
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/
#include "config.h"
#include "fshell.h"
#include <ctype.h>

#ifndef _WIN32
# include "linenoise.h"
# include <sys/types.h>
# include <sys/wait.h>
#endif


/*
** COMMAND: shell*
**
** Usage: %hyscm shell
**
** Prompt for lines of input from stdin.  Parse each line and evaluate
** it as a separate hyscm command, in a child process.  The initial
** "hyscm" is omitted from each line.
**
** This command only works on unix-like platforms that support fork().
** It is non-functional on Windows.
*/
void shell_cmd(void){
#ifdef _WIN32
  hyscm_fatal("the 'shell' command is not supported on windows");
#else
  int nArg;
  int mxArg = 0;
  int n, i;
  char **azArg = 0;
  int fDebug;
  pid_t childPid;
  char *zLine = 0;
  fDebug = find_option("debug", 0, 0)!=0;
  db_find_and_open_repository(OPEN_ANY_SCHEMA|OPEN_OK_NOT_FOUND, 0);
  db_close(0);
  sqlite3_shutdown();
  linenoiseSetMultiLine(1);
  while( (free(zLine), zLine = linenoise("hyscm> ")) ){
    /* Remember shell history within the current session */
    linenoiseHistoryAdd(zLine);

    /* Parse the line of input */
    n = (int)strlen(zLine);
    for(i=0, nArg=1; i<n; i++){
      while( hyscm_isspace(zLine[i]) ){ i++; }
      if( i>=n ) break;
      if( nArg>=mxArg ){
        mxArg = nArg+10;
        azArg = hyscm_realloc(azArg, sizeof(char*)*mxArg);
        if( nArg==1 ) azArg[0] = g.argv[0];
      }
      if( zLine[i]=='"' || zLine[i]=='\'' ){
        char cQuote = zLine[i];
        i++;
        azArg[nArg++] = &zLine[i];
        for(i++; i<n && zLine[i]!=cQuote; i++){}
      }else{
        azArg[nArg++] = &zLine[i];
        while( i<n && !isspace(zLine[i]) ){ i++; }
      }
      zLine[i] = 0;
    }

    /* If the --debug flag was used, display the parsed arguments */
    if( fDebug ){
      for(i=1; i<nArg; i++){
        hyscm_print("argv[%d] = [%s]\n", i, azArg[i]);
      }
    }

    /* Special cases */
    if( nArg<2 ) continue;
    if( hyscm_strcmp(azArg[1],"exit")==0 ) break;

    /* Fork a process to handle the command */
    childPid = fork();
    if( childPid<0 ){
      printf("could not fork a child process to handle the command\n");
      fflush(stdout);
      continue;
    }
    if( childPid==0 ){
      /* This is the child process */
      int main(int, char**);
      main(nArg, azArg);
      exit(0);
    }else{
      /* The parent process */
      int status;
      waitpid(childPid, &status, 0);
    }
  }
#endif
}
