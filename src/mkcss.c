/*
** HySCM
**
** Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
** Copyright (C) 2019 Hyang Language Foundation, Jakarta
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

static FILE *open_for_reading(const char *zFilename){
  FILE *f;
  if( strcmp(zFilename, "-")==0 ) return stdin;
  f = fopen(zFilename, "r");
  if( f==0 ){
    fprintf(stderr, "cannot open \"%s\" for reading\n", zFilename);
    exit(1);
  }
  return f;
}
static FILE *open_for_writing(const char *zFilename){
  FILE *f;
  if( strcmp(zFilename, "-")==0 ) return stdout;
  f = fopen(zFilename, "w");
  if( f==0 ){
    fprintf(stderr, "cannot open \"%s\" for writing\n", zFilename);
    exit(1);
  }
  return f;
}
static void close_file(FILE *f){
  if( f!=stdin && f!=stdout){
    fclose(f);
  }
}

/*
** Print a string as a quoted C-language string.
*/
static void clang_puts(FILE *out, const char *z){
  int i;
  while( z[0] ){
    for(i=0; z[i] && z[i]!='"' && z[i]!='\\'; i++){}
    fprintf(out, "%.*s", i, z);
    if( z[i] ){
      fprintf(out, "\\%c", z[i]);
      z += i+1;
    }else{
      z += i;
    }
  }
}

int main(int argc, char *argv[]){
  FILE *in, *out;
  int inRules = 0;
  int nLine = 0;
  int iStart = 0;
  const char *zInFile;
  const char *zOutFile;
  char z[1000];
  if( argc!=3 ){
    fprintf(stderr, "Usage: %s INPUTFILE OUTPUTFILE\n", argv[0]);
    return 1;
  }
  zInFile = argv[1];
  zOutFile = argv[2];
  in = open_for_reading(zInFile);
  out = open_for_writing(zOutFile);

  fprintf(out,
     "/* DO NOT EDIT\n"
     "** This code is generated automatically using 'mkcss.c'\n"
     "*/\n"
     "const struct strctCssDefaults {\n"
     "  const char *elementClass;  /* Name of element needed */\n"
     "  const char *value;         /* CSS text */\n"
     "} cssDefaultList[] = {\n"
  );
  while( fgets(z, sizeof(z), in) ){
    int n;  /* Line length */
    int i;
    nLine++;
    if( z[0]=='/' && z[1]=='/' ) continue;  /* Skip comments */
    if( z[0]=='-' && z[1]=='-' ) continue;  /* Skip comments */
    if( z[0]=='#' && !isalnum(z[1]) ) continue;  /* Skip comments */
    n = (int)strlen(z);
    while( n>0 && isspace(z[n-1]) ){ z[--n] = 0; }
    if( z[0]==0 ) continue;  /* Blank lines */
    if( isspace(z[0]) ){
      if( !inRules ){
        fprintf(stderr, "%s:%d: CSS rule not within a selector\n",
                zInFile, nLine);
        exit(1);
      }
      for(i=0; isspace(z[i]); i++){}
      fprintf(out, "    \"  ");
      clang_puts(out, z+i);
      fprintf(out, "\\n\"\n");
    }else if( z[0]=='}' ){
      if( !inRules ){
        fprintf(stderr, "%s:%d: surplus CSS rule terminator\n",
                zInFile, nLine);
        exit(1);
      }
      fprintf(out, "  },\n");
      inRules = 0;
    }else if( z[n-1]=='{' ){
      if( inRules ){
        fprintf(stderr, "%s:%d: selector where there should be rule\n",
                zInFile, nLine);
        exit(1);
      }
      inRules = 1;
      iStart = nLine;
      fprintf(out, "  { \"");
      n--;
      while( n>0 && isspace(z[n-1]) ){ z[--n] = 0; }
      clang_puts(out, z);
      fprintf(out, "\",\n");
    }else{
      fprintf(stderr, "%s:%d: syntax error\n",
              zInFile, nLine);
      exit(1);
    }
  }
  if( inRules ){
    fprintf(stderr, "%s:%d: unterminated CSS rule\n", zInFile, iStart);
    exit(1);
  }
  close_file(in);
  fprintf(out, "  {0,0}\n};\n");
  close_file(out);
  return 0;
}
