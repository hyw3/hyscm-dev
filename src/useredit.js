/* 
** Skrip ini digunakan pada halaman web /setup_uedit dimaksudkan agar
** kotak cek dengan hak akses pengguna serasi dengan string yang
** telah diperbarui (yang tersimpan di #usetupEditCapability)
*/
function updateCapabilityString(){
  try {
    var inputs = document.getElementsByTagName('input');
    if( inputs && inputs.length ){
      var output = document.getElementById('usetupEditCapability');
      if( output ){
        var permsIds = [], x = 0;
        for(var i = 0; i < inputs.length; i++){
          var e = inputs[i];
          if( !e.name || !e.type ) continue;
          if( e.type.toLowerCase()!=='checkbox' ) continue;
          if( e.name.length===2 && e.name[0]==='a' ){
            e.onchange = updateCapabilityString;
            if( e.checked ){
              permsIds[x++] = e.name[1];
            }
          }
        }
        permsIds.sort();
        output.innerHTML = permsIds.join('');
      }
    }
  } catch (e) {
  }
}
updateCapabilityString();
