/*
** HySCM
**
** Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
** Copyright (C) 2019 Hyang Language Foundation, Jakarta
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
*/
#include "config.h"
#include "loadctrl.h"
#include <assert.h>

/*
** Return the load average for the host processor
*/
double load_average(void){
#if !defined(_WIN32) && !defined(HYSCM_OMIT_LOAD_AVERAGE)
  double a[3];
  if( getloadavg(a, 3)>0 ){
    return a[0]>=0.000001 ? a[0] : 0.000001;
  }
#endif
  return 0.0;
}

/*
** COMMAND: test-loadavg
**
** %hyscm test-loadavg
**
** Print the load average on the host machine.
*/
void loadavg_test_cmd(void){
  hyscm_print("load-average: %f\n", load_average());
}

/*
** Abort the current operation of the load average of the host computer
** is too high.
*/
void load_control(void){
  double mxLoad = atof(db_get("max-loadavg", "0"));
  if( mxLoad<=0.0 || mxLoad>=load_average() ) return;

  style_header("Server Overload");
  @ <h2>The server load is currently too high.
  @ Please try again later.</h2>
  @ <p>Current load average: %f(load_average()).<br />
  @ Load average limit: %f(mxLoad)</p>
  style_footer();
  cgi_set_status(503,"Server Overload");
  cgi_reply();
  exit(0);
}
