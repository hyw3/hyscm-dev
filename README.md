# About HySCM

HySCM is a source code management system which supports distributed version control,
autosync, and built based on blockchain concepts and principles. HySCM originally
designed to support cLHy web server and other Hyang projects. But it's also working
using HySCM built-in (stand-alone) server or within other popular web servers.

HySCM's homepage at <http://hyscm.hyang.org/>

The HySCM source codes are also available at:

Git repository at Bitbucket <https://bitbucket.org/hyw3/hyscm-dev>

SVN repository at OSDN <https://osdn.net/projects/hyscm-dev/>

For bug reporting, send to the email address 'hyangcontrib@gmail.com' if the email
address 'contributing@hyang.org' cannot be reached at the time.
