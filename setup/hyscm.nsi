
; The name of the installer
Name "HySCM"

; The file to write
OutFile "hyscm-setup.exe"

; The default installation directory
InstallDir $PROGRAMFILES\HySCM
; Registry key to check for directory (so if you install again, it will
; overwrite the old one automatically)
InstallDirRegKey HKLM SOFTWARE\HySCM "Install_Dir"

; The text to prompt the user to enter a directory
ComponentText "This will install hyscm on your computer."
; The text to prompt the user to enter a directory
DirText "Choose a directory to install in to:"

; The stuff to install
Section "HySCM (required)"
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  ; Put file there
  File "..\hyscm.exe"
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\HySCM "Install_Dir" "$INSTDIR"
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HySCM" "DisplayName" "HySCM (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HySCM" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteUninstaller "uninstall.exe"
SectionEnd


; uninstall stuff

UninstallText "This will uninstall hyscm. Hit next to continue."

; special uninstall section.
Section "Uninstall"
  ; remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\HySCM"
  DeleteRegKey HKLM SOFTWARE\HySCM
  ; remove files
  Delete $INSTDIR\hyscm.exe
  ; MUST REMOVE UNINSTALLER, too
  Delete $INSTDIR\uninstall.exe
  ; remove shortcuts, if any.
  RMDir "$SMPROGRAMS\HySCM"
  RMDir "$INSTDIR"
SectionEnd

; eof
