;
; HySCM
;
; Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
; Copyright (C) 2019 Hyang Language Foundation, Jakarta
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

[Setup]
ArchitecturesAllowed=x86 x64
AlwaysShowComponentsList=false
AppCopyright=Copyright (c) 2019 The Hyang Language Foundation.  All rights reserved.
AppID={{tbc25a1f-3674-4bha-aw76-2914c52f059f}
AppName=HySCM
AppPublisher=HySCM Development Team
AppPublisherURL=http://hyscm.hyang.org/
AppSupportURL=http://hyscm.hyang.org/
AppUpdatesURL=http://hyscm.hyang.org/
AppVerName=HySCM v{#AppVersion}
AppVersion={#AppVersion}
AppComments=High-reliability, blockchain-based, distributed version control management system.
AppReadmeFile=http://hyscm.hyang.org/
DefaultDirName={pf}\HySCM
DefaultGroupName=HySCM
OutputBaseFilename=hyscm-win32-{#AppVersion}
OutputHyscmtermFile=hyscm-win32-{#AppVersion}-hyscmterm.txt
SetupLogging=true
UninstallFilesDir={app}\uninstall
VersionInfoVersion={#AppVersion}

[Components]
Name: Application; Description: Core application.; Types: custom compact full; Flags: fixed

[Dirs]
Name: {app}\bin

[Files]
Components: Application; Source: ..\hyscm.exe; DestDir: {app}\bin; Flags: restartreplace uninsrestartdelete

[Registry]
Components: Application; Root: HKLM32; SubKey: Software\HySCM; ValueType: string; ValueName: Install_Dir; ValueData: {app}; Flags: uninsdeletekeyifempty uninsdeletevalue
