body {
    margin: 0 auto;
    background-color: #4d2600;
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
    font-size:14pt;
    -moz-text-size-adjust: none;
    -webkit-text-size-adjust: none;
    -mx-text-size-adjust: none;
}

a {
    color: #ff8c1a;
    text-decoration: none;
}

a:hover {
    color: #ff8000;
    text-decoration: underline;
}

div.forumPosts a:visited {
    color: #e67300;
}

hr {
    color: #cc6600;
}

.title {
    color: #cc6600;
    float:left;
}
.title h1 {
    display:inline;
}
.title h1:after {
    content: " / ";
    color: #b35900;
    font-weight: normal;
}

.content h1 {
    font-size: 1.25em;
}
.content h2 {
    font-size: 1.15em;
}
.content h3 {
    font-size: 1.05em;
    font-weight: bold;
}

.section {
    font-size: 1em;
    font-weight: bold;
    border-bottom: 1px dashed #ff8c1a;
    padding: 9px 10px 10px;
    margin: 10px 0;
}

.sectionmenu {
    border: 1px dashed #ff8c1a;
    border-radius: 0 0 5px 5px;
    border-top: 0;
    margin-top: -10px;
    margin-bottom: 10px;
    padding: 10px;
}

.sectionmenu a {
    display: inline-block;
    margin-right: 1em;
}

.sectionmenu a:hover {
    text-decoration: none;
    color: #ffcc99;
}

.status {
    float:right;
    font-size:.7em;
    background-color: #cc6600;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    padding: 2px 6px;
    display: block;
}

.status a{
    background-color: #cc6600;
    color: #ffd9b3;
}

.status a:hover{
    color: #fff2e6;
    text-decoration: none;
}

.mainmenu {
    font-size:.8em;
    clear:both;
    background:#cc6600;
    border-top-left-radius:5px;
    border-top-right-radius:5px;
    overflow-x: auto;
    overflow-y: hidden;
    white-space: nowrap;
    z-index: 21;
}

.mainmenu a {
    text-decoration:none;
    color: #ffcc99;
}
.mainmenu a.active,
.mainmenu a:hover {
    color: #ffe6cc;
}

div#hbdrop {
    background-color: #663500;
    color: #ffce99;
    border: 1px solid black;
    border-top: white;
    border-radius: 0 0 0.5em 0.5em;
    display: none;
    font-size: 80%;
    left: 2em;
    width: 90%;
    padding-right: 1em;
    position: absolute;
    z-index: 20; 
}

.submenu {
    font-size: .7em;
    padding: 10px;
    border-right: 1px dashed #cc6600;
    border-bottom-right-radius: 5px;
    border-left: 1px dashed #cc6600;
    border-bottom-left-radius: 5px;
    border-bottom: 1px dashed #cc6600;
}

.submenu a, .submenu label {
    padding: 10px 11px;
    text-decoration:none;
    color: #ffd9b3;
}

.submenu a:hover, .submenu label:hover {
    padding: 6px 10px;
    border-top: 1px dashed #ffe6cc;
    color: #ffd9b3;
}

.content {
    padding-top: 10px;
    font-size:.8em;
    color: #ffb366;
}

table.sbsdiffcols pre{
   color: #ff8c1a;
   font-size:1.5em;
}

.udiff, .sbsdiff {
    font-size: .85em !important;
    overflow: auto;
    border: 1px solid #ccc;
    border-radius: 5px;
}
.content blockquote {
    padding: 0 15px;
}
div.forumHierRoot blockquote, div.forumHier blockquote, div.forumEdit blockquote {
    background-color: rgba(65, 131, 196, 0.1);
    border-left: 3px solid #254769;
    padding: .1em 1em;
}

table.report {
    cursor: auto;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin: 1em 0;
}
.report td, .report th {
   border: 0;
   font-size: .8em;
   padding: 10px;
}
.report td:first-child {
    border-top-left-radius: 5px;
}
.report tbody tr:last-child td:first-child {
    border-bottom-left-radius: 5px;
}
.report td:last-child {
    border-top-right-radius: 5px;
}
.report tbody tr:last-child {
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
}
.report tbody tr:last-child td:last-child {
    border-bottom-right-radius: 5px;
}
.report th {
    cursor: pointer;
}
.report thead+tbody tr:hover {
    background-color: #f5f9fc !important;
}

td.tktDspLabel {
    width: 70px;
    text-align: right;
    overflow: hidden;
}
td.tktDspValue {
    text-align: left;
    vertical-align: top;
    background-color: #f8f8f8;
    border: 1px solid #ccc;
}
td.tktDspValue pre {
    white-space: pre-wrap;
}

span.timelineDetail {
    font-size: 90%;
}

tr.timelineCurrent{
   background-color:#4d2600;
}

tr.timelineSelected{
    background-color:#4d2600;
}

.timelineModernCell,
.timelineColumnarCell,
.timelineDetailCell,
.timelineCompactCell,
.timelineVerboseCell {
  vertical-align: top; 
  text-align: left;
  padding: .75em;
  border-radius: 5px;
  background: #663300;
}

.timelineSelected > .timelineColumnarCell,
.timelineSelected > .timelineCompactCell,
.timelineSelected > .timelineDetailCell,
.timelineSelected > .timelineModernCell,
.timelineSelected > .timelineVerboseCell {
   padding: .75em;
   border-radius: 5px;
   border: 1px dashed #ff8c1a;
   vertical-align: top;
   text-align: left;
   background: #663300;
}

.timelineCurrent > .timelineColumnarCell,
.timelineCurrent > .timelineCompactCell,
.timelineCurrent > .timelineDetailCell,
.timelineCurrent > .timelineModernCell,
.timelineCurrent > .timelineVerboseCell {
    vertical-align: top;
    text-align: left;
    padding: .75em;
    border-radius: 5px;
    border: 1px dashed #ff8c1a;
}

.timelineModernCell[id], .timelineColumnarCell[id], .timelineDetailCell[id] {
   background-color: #663300;
} 

.footer {
    border-top: 1px dashed #ff8c1a;
    padding: 10px;
    font-size:.7em;
    margin-top: 10px;
    color: #cc6600;
}

div.timelineDate {
    font-weight: bold;
    white-space: nowrap;
}

select.submenuctrl {
  background-color: #4d2600;
  color: #ff9933;
  border-radius: 5px;
  border: 1px solid #cc6600;
  margin-right: 4px;
}

input{
  margin-left: 4px;
  background-color: #663300;
  color: #fff2e6;
  border-radius: 5px;
  border: 1px solid #cc6600;
}

textarea{
  margin-left: 4px;
  background-color: #663300;
  color: #fff2e6;
  border-radius: 5px;
  border: 1px solid #cc6600;
}

select {
    background-color: #4d2600;
    color:#ff9933;
    border-radius: 5px;
    border: 1px solid#cc6600;
    margin-right: 4px;
}

span.submenuctrl {
  white-space: nowrap;
  margin-right: 4px;
  color: #ffd9b3;
}

div.submenu label {
  white-space: nowrap;
}

@media screen and (max-width: 600px) {
  /* Spacing for mobile */
  body {
    padding-left: 4px;
    padding-right: 4px;
  }
  .title {
    padding-top: 0px;
    padding-bottom: 0px;
  }
  .status {padding-top: 0px;}
  .mainmenu a {
    padding: 10px 10px;
  }
  .mainmenu {
    padding: 10px;
  }
  .desktoponly {
    display: none;
  }
}
@media screen and (min-width: 600px) {
  /* Spacing for desktop */
  body {
    padding-left: 20px;
    padding-right: 20px;
  }
  .title {
    padding-top: 10px;
    padding-bottom: 10px;
  }
  .status {padding-top: 30px;}
  .mainmenu a {
    padding: 10px 20px;
  }
  .mainmenu {
    padding: 10px;
  }
}
@media screen and (max-width: 1200px) {
  .wideonly {
    display: none;
  }
}

