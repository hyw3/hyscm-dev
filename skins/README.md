Tema Bawaan
===========

Tiap subdirektori di folder berisi "tema" bawaan. Ada empat berkas di setiap subdirektori
yaitu untuk berkas CSS, berkas rincian, footer, dan header.

Untuk mengedit tema bawaan yang ada, cukup edit berkas yang sesuai dan kompilasi ulang.

Untuk menambah tema baru:

   1.   Buat subdirectori baru pada skins/.

   2.   Tambahkan berkas-berkas berikut: skins/temabaru/css.txt, skins/temabaru/details.txt,
        skins/temabaru/footer.txt dan skins/temabaru/header.txt.

   3.   Masuk ke direktori src/ dan jalankan kembali "tclsh buildmake.tcl".
        Langkah ini akan membangun kembali berkas-berkas makefile.

   4.   Edit pada array aBuiltinSkin[] dekat bagian atas kode sumber pada berkas src/hyscmskins.c
        sehingga ia menggambarkan dan mereferensikan "temabaru" tadi.

   5.   Jalankan "make" untuk membangun ulang.

Petunjuk Pengembangan
---------------------

Salah satu sara untuk mengembangkan tema baru adalah menyalin berkas baseline (css.txt,
details.txt, footer.txt, and header.txt) ke direktori kerja saat ini $WORKDIR
kemudian luncurkan HySCM dengan opsi baris-perintah "--skin $WORKDIR".  Misalnya:

        cp -r skins/default newskin
        hyscm ui --skin ./newskin

Ketika argumen ke --skin mengandung satu atau lebih karakter '/', berkas-berkas tema yang
bersesuaian akan membacanya. Sehingga setelah meluncurkan hyscm, Anda dapat mengedit
temabaru/css.txt, temabaru/details.txt, temabaru/footer.txt, dan temabaru/header.txt
menggunakan editor teks, kemudian tekan Reload pada web browser untuk melihat efeknya.

