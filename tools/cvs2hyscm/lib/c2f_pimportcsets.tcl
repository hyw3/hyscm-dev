## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                                   ; # Required runtime.
package require snit                                      ; # OO system.
package require vc::tools::log                            ; # User feedback.
package require vc::hyscm::import::cvs::repository       ; # Repository management.
package require vc::hyscm::import::cvs::state            ; # State storage.
package require vc::hyscm::import::cvs::hyscm           ; # Access to hyscm repositories.
package require vc::hyscm::import::cvs::ristate          ; # Import state (revisions)

# # ## ### ##### ######## ############# #####################
## Register the pass with the management

vc::hyscm::import::cvs::pass define \
    ImportCSets \
    {Import the changesets into hyscm repositories} \
    ::vc::hyscm::import::cvs::pass::importcsets

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::pass::importcsets {
    # # ## ### ##### ######## #############
    ## Public API

    typemethod setup {} {
	# Define the names and structure of the persistent state of
	# this pass.

	state use project
	state use file
	state use revision
	state use meta
	state use author
	state use cmessage
	state use symbol
	state use space
	state use revuuid
	return
    }

    typemethod load {} {
	# Pass manager interface. Executed to load data computed by
	# this pass into memory when this pass is skipped instead of
	# executed.
	return
    }

    typemethod run {} {
	# Pass manager interface. Executed to perform the
	# functionality of the pass.

	foreach project [repository projects] {
	    log write 1 importcsets {Importing project "[$project base]"}

	    set pid    [$project id]
	    set hyscm [hyscm %AUTO%]
	    struct::list assign [state run {
		SELECT repository, workspace
		FROM space
		WHERE pid = $pid
	    }] r w
	    $hyscm load $r $w

	    set rstate [ristate %AUTO%]

	    state transaction {
		# Layer II: Changesets
		foreach {cset date} [$project changesetsinorder] {
		    $cset pushto $hyscm $date $rstate
		}
	    }

	    $rstate destroy
	    $hyscm destroy
	}
	return
    }

    typemethod discard {} {
	# Pass manager interface. Executed for all passes after the
	# run passes, to remove all data of this pass from the state,
	# as being out of date.
	return
    }

    # # ## ### ##### ######## #############
    ## Internal methods

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hasinstances   no ; # singleton
    pragma -hastypeinfo    no ; # no introspection
    pragma -hastypedestroy no ; # immortal

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs::pass {
    namespace export importcsets
    namespace eval importcsets {
	namespace import ::vc::hyscm::import::cvs::repository
	namespace import ::vc::hyscm::import::cvs::state
	namespace import ::vc::hyscm::import::cvs::hyscm
	namespace import ::vc::hyscm::import::cvs::ristate
	namespace import ::vc::tools::log
	log register importcsets
    }
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::pass::importcsets 1.0
return
