## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                                   ; # Required runtime.
package require snit                                      ; # OO system.
package require vc::tools::log                            ; # User feedback.
package require vc::hyscm::import::cvs::repository       ; # Repository management.
package require vc::hyscm::import::cvs::state            ; # State storage.
package require vc::hyscm::import::cvs::hyscm           ; # Access to hyscm repositories.

# # ## ### ##### ######## ############# #####################
## Register the pass with the management

vc::hyscm::import::cvs::pass define \
    ImportFiles \
    {Import the file revisions into hyscm repositories} \
    ::vc::hyscm::import::cvs::pass::importfiles

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::pass::importfiles {
    # # ## ### ##### ######## #############
    ## Public API

    typemethod setup {} {
	# Define the names and structure of the persistent state of
	# this pass.

	state use project
	state use file
	state use revision
	state use meta
	state use author
	state use cmessage
	state use symbol

	# Discard on setup. Do not discard on deferal.
	state discard revuuid
	state extend  revuuid {
	    rid   INTEGER NOT NULL  REFERENCES revision UNIQUE,
	    uuid  INTEGER NOT NULL  -- hyscm id of the revision
	    --                         unique within the project
	}

	# Remember the locations of the scratch data createdby this
	# pass, for use by the next (importing changesets).
	state discard space
	state extend  space {
	    pid        INTEGER  NOT NULL  REFERENCES project,
	    repository TEXT     NOT NULL,
	    workspace  TEXT     NOT NULL
	}
	return
    }

    typemethod load {} {
	# Pass manager interface. Executed to load data computed by
	# this pass into memory when this pass is skipped instead of
	# executed.
	return
    }

    typemethod run {} {
	# Pass manager interface. Executed to perform the
	# functionality of the pass.

	foreach project [repository projects] {
	    log write 1 importfiles {Importing project "[$project base]"}

	    set pid    [$project id]
	    set hyscm [hyscm %AUTO%]
	    $hyscm initialize

	    state transaction {
		# Layer I: Files and their revisions
		foreach file [$project files] {
		    $file pushto $hyscm
		}

		# Save the scratch locations, needed by the next pass.
		struct::list assign [$hyscm space] r w
		state run {
		    DELETE FROM space
		    WHERE pid = $pid
		    ;
		    INSERT INTO space (pid, repository, workspace)
		    VALUES            ($pid, $r, $w);
		}
	    }

	    $hyscm destroy
	}
	return
    }

    typemethod discard {} {
	# Pass manager interface. Executed for all passes after the
	# run passes, to remove all data of this pass from the state,
	# as being out of date.

	# Not discarding revuuid/space here, allow us to keep the info
	# for the next pass even if the revisions are recomputed.
	return
    }

    # # ## ### ##### ######## #############
    ## Internal methods

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hasinstances   no ; # singleton
    pragma -hastypeinfo    no ; # no introspection
    pragma -hastypedestroy no ; # immortal

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs::pass {
    namespace export importfiles
    namespace eval importfiles {
	namespace import ::vc::hyscm::import::cvs::repository
	namespace import ::vc::hyscm::import::cvs::state
	namespace import ::vc::hyscm::import::cvs::hyscm
	namespace import ::vc::tools::log
	log register importfiles
    }
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::pass::importfiles 1.0
return
