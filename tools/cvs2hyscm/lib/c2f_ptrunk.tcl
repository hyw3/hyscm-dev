## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                             ; # Required runtime.
package require snit                                ; # OO system.

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::project::trunk {
    # # ## ### ##### ######## #############
    ## Public API

    constructor {project} {
	set mysymbol [$project getsymbol $myname]
	set myid     [$mysymbol id]
	return
    }

    destructor {
	$mysymbol destroy
    }

    method name    {} { return $myname }
    method id      {} { return $myid   }
    method istrunk {} { return 1 }
    method symbol  {} { return $self }
    method parent  {} { return $self }

    method forceid {id} { set myid $id ; return }

    method defcounts {tc bc cc} {}

    method countasbranch {} {}
    method countastag    {} {}
    method countacommit  {} {}

    method blockedby      {symbol} {}
    method possibleparent {symbol} {}

    method isghost {} { return 0 }

    delegate method persistrev to mysymbol

    method determinetype {} { $mysymbol markthetrunk }

    # # ## ### ##### ######## #############
    ## State

    typevariable myname   :trunk: ; # Name shared by all trunk symbols.
    variable     myid     {}      ; # The trunk's symbol id.
    variable     mysymbol {}      ; # The symbol underneath the trunk.

    # # ## ### ##### ######## #############
    ## Internal methods

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hastypeinfo    no  ; # no type introspection
    pragma -hasinfo        no  ; # no object introspection
    pragma -hastypemethods no  ; # type is not relevant.

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs::project {
    namespace export trunk
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::project::trunk 1.0
return
