## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                               ; # Required runtime.
package require snit                                  ; # OO system.
package require struct::list                          ; # List assignment
package require vc::hyscm::import::cvs::wsstate      ; # Workspace state
package require vc::hyscm::import::cvs::integrity    ; # State integrity checks.
package require vc::tools::log                        ; # User feedback.
package require vc::tools::trouble                    ; # Error reporting.

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::ristate {
    # # ## ### ##### ######## #############
    ## Public API

    constructor {} {
	# Start with an empty state
	return
    }

    method new {lod {parentlod {}}} {
	# Create a workspace for a line of development (LOD). If a
	# parent LOD is specified let the new workspace inherit the
	# current state of the parent.

	log write 8 ristate {Open workspace for LOD "$lod"}

	integrity assert {
	    ![info exists mystate($lod)]
	} {Trying to override existing state for lod "$lod"}

	set wss [wsstate ${selfns}::%AUTO% $lod]
	set mystate($lod) $wss

	if {$parentlod ne ""} {
	    log write 8 ristate {Inheriting from workspace for LOD "$parentlod"}

	    integrity assert {
		[info exists mystate($parentlod)]
	    } {Trying to inherit from undefined lod "$parentlod"}

	    set pwss $mystate($parentlod)

	    $wss defstate  [$pwss getstate]
	    $wss defid     [$pwss getid]
	    $wss defparent $pwss
	}

	return $wss
    }

    method get {lod} { return $mystate($lod) }
    method has {lod} { return [info exists mystate($lod)] }

    method names {} { return [array names mystate] }

    method dup {dst _from_ src} {
	log write 8 ristate {Duplicate workspace for LOD "$dst" from "$src"}
	set mystate($dst) $mystate($src)
	return
    }

    # # ## ### ##### ######## #############
    ## State

    variable mystate -array {} ; # Map from lines of development
				 # (identified by name) to their
				 # workspace.

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hastypeinfo    no  ; # no type introspection
    pragma -hasinfo        no  ; # no object introspection
    pragma -hastypemethods no  ; # type is not relevant.

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs {
    namespace export ristate
    namespace eval ristate {
	namespace import ::vc::hyscm::import::cvs::wsstate
	namespace import ::vc::hyscm::import::cvs::integrity
	namespace import ::vc::tools::trouble
	namespace import ::vc::tools::log
	log register ristate
    }
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::ristate 1.0
return
