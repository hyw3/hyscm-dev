## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                             ; # Required runtime.
package require snit                                ; # OO system.
package require struct::list                        ; # List assignment
package require vc::tools::log                      ; # User feedback.

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::wsstate {
    # # ## ### ##### ######## #############
    ## Public API

    constructor {lod} {
	# Start with an empty state
	set myname   $lod
	set myticks  0
	set myparent {}
	return
    }

    method name   {} { return $myname }
    method ticks  {} { return $myticks }

    method add {oprevisioninfo} {
	# oprevisioninfo = list (rid path label op ...) /quadruples

	# Overwrite all changed files (identified by path) with the
	# new revisions. This keeps all unchanged files. Files marked
	# as dead are removed.

	foreach {rid path label rop} $oprevisioninfo {
	    log write 5 wss {$myop($rop) $label}

	    if {$rop < 0} {
		if {[catch {
		    unset mystate($path)
		}]} {
		    log write 0 wss "Removed path \"$path\" is not known to the workspace"
		}
	    } else {
		set mystate($path) [list $rid $label]
	    }
	}

	incr myticks
	return
    }

    method get {} {
	set res {}
	foreach path [lsort -dict [array names mystate]] {
	    struct::list assign $mystate($path) rid label
	    lappend res $rid $path $label
	}
	return $res
    }

    method defid {id} {
	set myid $id
	return
    }

    method getid {} { return $myid }

    method defstate {s} { array set mystate $s ; return }
    method getstate {}  { return [array get mystate] }

    method parent {} { return $myparent }
    method defparent {parent} {
	set myparent $parent
	return
    }

    # # ## ### ##### ######## #############
    ## State

    variable myname {}         ; # Name of the LOD the workspace is
				 # for.
    variable myid   {}         ; # Record id of the hyscm hyscmterm
				 # associated with the current state.
    variable mystate -array {} ; # Map from paths to the recordid of
				 # the file revision behind it, and
				 # the associated label for logging.
    variable myticks 0         ; # Number of 'add' operations
				 # performed on the state.
    variable myparent {}       ; # Reference to the parent workspace.

    typevariable myop -array {
	-1 REM
	0  ---
	1  ADD
	2  CHG
    }

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hastypeinfo    no  ; # no type introspection
    pragma -hasinfo        no  ; # no object introspection
    pragma -hastypemethods no  ; # type is not relevant.

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs {
    namespace export wsstate
    namespace eval wsstate {
	namespace import ::vc::tools::log
	log register wss
    }
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::wsstate 1.0
return
