## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                             ; # Required runtime.
package require snit                                ; # OO system.

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::file::trunk {
    # # ## ### ##### ######## #############
    ## Public API

    constructor {} {
	return
    }

    # # ## ### ##### ######## #############
    ## State

    # # ## ### ##### ######## #############
    ## Internal methods

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hastypeinfo    no  ; # no type introspection
    pragma -hasinfo        no  ; # no object introspection
    pragma -hastypemethods no  ; # type is not relevant.
    pragma -simpledispatch yes ; # simple fast dispatch

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs::file {
    namespace export trunk
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::file::trunk 1.0
return
