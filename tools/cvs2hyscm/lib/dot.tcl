## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4  ; # Required runtime
package require snit     ; # OO system.
package require fileutil ; # Helper commands.

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::tools::dot {
    # # ## ### ##### ######## #############
    ## Public API, Methods

    typemethod format {g name {subgraph {}}} {
	lappend lines "digraph \"$name\" \{"

	if {![llength $subgraph]} {
	    set nodes [$g nodes]
	    set arcs  [$g arcs]
	} else {
	    set nodes $subgraph
	    set arcs [eval [linsert $subgraph 0 $g arcs -inner]]
	}

	foreach n $nodes {
	    set style [Style $g node $n {label label shape shape fontcolor fontcolor}]
	    lappend lines "\"$n\" ${style};"
	}
	foreach a $arcs {
	    set style [Style $g arc $a {color color}]
	    lappend lines "\"[$g arc source $a]\" -> \"[$g arc target $a]\" ${style};"
	}

	lappend lines "\}"
	return [join $lines \n]
    }

    typemethod write {g name file {subgraph {}}} {
	fileutil::writeFile $file [$type format $g $name $subgraph]
	return
    }

    typemethod layout {format g name file} {
	set f [fileutil::tempfile c2fdot_]
	$type write $g $name $f
	exec dot -T $format -o $file $f
	::file delete $f
	return
    }

    # # ## ### ##### ######## #############
    ## Internal, state

    proc Style {graph x y dict} {
	set sep " "
	set head " \["
	set tail ""
	set style ""
	foreach {gattr key} $dict {
	    if {![$graph $x keyexists $y $key]} continue
	    append style "$head$sep${gattr}=\"[$graph $x get $y $key]\""
	    set sep ", "
	    set head ""
	    set tail " \]"
	}

	append style ${tail}
	return $style
    }

    # # ## ### ##### ######## #############
    ## Internal, helper methods (formatting, dispatch)

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hasinstances   no ; # singleton
    pragma -hastypeinfo    no ; # no introspection
    pragma -hastypedestroy no ; # immortal

    # # ## ### ##### ######## #############
}

namespace eval ::vc::tools {
    namespace export dot
}

# -----------------------------------------------------------------------------
# Ready

package provide vc::tools::dot 1.0
return
