## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

# # ## ### ##### ######## ############# #####################
## Requirements

package require Tcl 8.4                             ; # Required runtime.
package require fileutil                            ; # Temp.dir/file
package require snit                                ; # OO system.
package require vc::tools::trouble                  ; # Error reporting.
package require vc::tools::log                      ; # User feedback
package require vc::hyscm::import::cvs::integrity  ; # State integrity checks.

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::hyscm {
    # # ## ### ##### ######## #############
    ## Public API

    constructor {} {
	return
    }

    method initialize {} {
	set myrepository [fileutil::tempfile cvs2hyscm_repo_]
	set myworkspace  [fileutil::tempfile cvs2hyscm_wspc_]
	::file delete $myworkspace
	::file mkdir  $myworkspace

	Do new [::file nativename $myrepository]
	$self InWorkspace ; Do open [::file nativename $myrepository]
	$self RestorePwd

	log write 8 hyscm {Scratch repository created @ $myrepository}
	log write 8 hyscm {Scratch workspace  created @ $myworkspace }
	return
    }

    method load {r w} {
	set myrepository $r
	set myworkspace  $w

	log write 8 hyscm {Scratch repository found @ $myrepository}
	log write 8 hyscm {Scratch workspace  found @ $myworkspace}
	return
    }

    method space {} {
	return [list $myrepository $myworkspace]
    }

    # # ## ### ##### ######## #############
    ##

    method root {} {
	# The id of the root hyscmterm is hardwired into hyscm. This
	# hyscmterm is created when a new repository is made (See
	# 'new', in the constructor).
	return 1
    }

    method workspace {} { return $myworkspace }

    method importfiles {map} {
	# map = list (instruction), instruction = add|delta
	# add   = list ('A', path)
	# delta = list ('D', path, src)

	log write 3 hyscm {Importing revisions...}

	array set id {}
	$self InWorkspace

	set n   0
	set max [llength $map]

	foreach insn $map {
	    log progress 3 hyscm $n $max ; incr n

	    struct::list assign $insn cmd pa pb
	    switch -exact -- $cmd {
		A {
		    log write 8 hyscm {Importing   <$pa>,}

		    # Result = 'inserted as record :FOO:'
		    #           0        1  2     3
		    set res [Do test-content-put $pa]
		    integrity assert {
			[regexp {^inserted as record \d+$} $res]
		    } {Unable to process unexpected hyscm output '$res'}
		    set id($pa) [lindex $res 3]
		}
		D {
		    log write 8 hyscm {Compressing <$pa>, as delta of <$pb>}

		    Do test-content-deltify $id($pa) $id($pb) 1
		}
	    }
	}
	$self RestorePwd

	log write 3 hyscm Done.
	return [array get id]
    }

    method importrevision {label user message date parent revisions} {
	# Massage the commit message to remember the old user name
	# which did the commit in CVS.

	set message "By $user:\n$message"

	log write 2 hyscm {== $user @ [clock format $date]}
	log write 2 hyscm {-> $parent}
	log write 9 hyscm {%% [join [split $message \n] "\n%% "]}

	lappend cmd Do test-import-hyscmterm $date $message
	if {$parent ne ""} { lappend cmd -p $parent }
	foreach {frid fpath flabel} $revisions {
	    lappend cmd -f $frid $fpath
	    log write 12 hyscm {** <[format %5d $frid]> = <$flabel>}
	}

	# run hyscm test-command performing the import.
	log write 12 hyscm {	[lreplace $cmd 3 3 @@]}

	$self InWorkspace
	set res [eval $cmd]
	$self RestorePwd

	integrity assert {
	    [regexp {^inserted as record \d+, [0-9a-fA-F]+$} $res]
	} {Unable to process unexpected hyscm output '$res'}
	set rid  [string trim [lindex $res 3] ,]
	set uuid [lindex $res 4]

	log write 2 hyscm {== $rid ($uuid)}

	return [list $rid $uuid]
    }

    method tag {uuid name} {
	log write 2 hyscm {Tag '$name' @ $uuid}

	$self InWorkspace
	Do tag add sym-$name $uuid
	$self RestorePwd
	return
    }

    method branchmark {uuid name} {
	# We do not mark the trunk
	if {$name eq ":trunk:"} return

	log write 2 hyscm {Begin branch '$name' @ $uuid}

	$self InWorkspace
	Do tag branch sym-$name $uuid
	$self RestorePwd
	return
    }

    method branchcancel {uuid name} {
	# The trunk is unmarked, thus cancellation is not needed
	# either.
	if {$name eq ":trunk:"} return

	log write 2 hyscm {Cancel branch '$name' @ $uuid}

	$self InWorkspace
	Do tag delete sym-$name $uuid
	$self RestorePwd
	return
    }

    method finalize {destination} {
	log write 2 hyscm {Finalize, rebuilding repository}
	Do rebuild [::file nativename $myrepository]

	::file rename -force $myrepository $destination
	::file delete -force $myworkspace
	$self destroy

	log write 2 hyscm {destination $destination}
	return
    }

    # # ## ### ##### ######## #############
    ##

    typemethod setlocation {path} {
	set myhyscmcmd    $path
	set myneedlocation 0
	return
    }

    typemethod validate {} {
	if {!$myneedlocation} {
	    if {![fileutil::test $myhyscmcmd efrx msg]} {
		trouble fatal "Bad path for hyscm executable: $msg"
	    }
	} else {
	    trouble fatal "Don't know where to find the 'hyscm' executable"
	}
	return
    }

    typeconstructor {
	set location [auto_execok hyscm]
	set myneedlocation [expr {$location eq ""}]
	if {$myneedlocation} return
	$type setlocation $location
	return
    }

    # # ## ### ##### ######## #############
    ## State

    variable mypwd        {} ; # Path to last CWD
    variable myrepository {} ; # Path to our hyscm database.
    variable myworkspace  {} ; # Path to the workspace for our hyscm
			       # database.

    typevariable myhyscmcmd    ; # Path to hyscm executable.
    typevariable myneedlocation ; # Boolean, indicates if user has to
				  # tell us where hyscm lives or not.

    # # ## ### ##### ######## #############
    ## Internal methods

    proc Do {args} {
	# 8.5: exec $myhyscmcmd {*}$args
	log write 14 hyscm {Doing '$args'}
	return [eval [linsert $args 0 exec $myhyscmcmd]]
    }

    method InWorkspace {} { set mypwd [pwd] ; cd $myworkspace ; return }
    method RestorePwd  {} { cd $mypwd       ; set mypwd {}    ; return }

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hastypeinfo    no  ; # no type introspection
    pragma -hasinfo        no  ; # no object introspection

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs {
    namespace export hyscm
    namespace eval   hyscm {
	namespace import ::vc::tools::trouble
	namespace import ::vc::tools::log
	namespace import ::vc::hyscm::import::cvs::integrity
    }
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::hyscm 1.0
return
