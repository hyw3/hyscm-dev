## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                               ; # Required runtime.
package require snit                                  ; # OO system.
package require vc::tools::misc                       ; # Text formatting.
package require vc::tools::log                        ; # User feedback.
package require vc::hyscm::import::cvs::state        ; # State storage.
package require vc::hyscm::import::cvs::project::rev ; # Project level changesets

# # ## ### ##### ######## ############# #####################
## Register the pass with the management

vc::hyscm::import::cvs::pass define \
    CsetDeps \
    {Compute and cache ChangeSet Dependencies} \
    ::vc::hyscm::import::cvs::pass::csetdeps

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs::pass::csetdeps {
    # # ## ### ##### ######## #############
    ## Public API

    typemethod setup {} {
	# Define the names and structure of the persistent state of
	# this pass.

	state use project
	state use file
	state use revision
	state use revisionbranchchildren
	state use branch
	state use tag
	state use symbol
	state use preferedparent
	state use changeset
	state use csitem

	# A table listing for each changeset the set of successor
	# changesets. The predecessor information is implied.

	state extend cssuccessor {
	    cid  INTEGER  NOT NULL  REFERENCES changeset,
	    nid  INTEGER  NOT NULL  REFERENCES changeset,
	    UNIQUE (cid,nid)
	} {cid nid}
	# Index on both columns for fast forward and back retrieval.
	return
    }

    typemethod load {} {
	# Pass manager interface. Executed to load data computed by
	# this pass into memory when this pass is skipped instead of
	# executed.

	state use cssuccessor
	return
    }

    typemethod run {} {
	# Pass manager interface. Executed to perform the
	# functionality of the pass.

	set n   0
	set max [llength [project::rev all]]

	foreach cset [project::rev all] {
	    log progress 2 csetdeps $n $max
	    # NOTE: Consider to commit only every N calls.
	    state transaction {
		$cset determinesuccessors
	    }
	    incr n
	}
	return
    }

    typemethod discard {} {
	# Pass manager interface. Executed for all passes after the
	# run passes, to remove all data of this pass from the state,
	# as being out of date.

	state discard cssuccessor
	return
    }

    # # ## ### ##### ######## #############
    ## Internal methods

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hasinstances   no ; # singleton
    pragma -hastypeinfo    no ; # no introspection
    pragma -hastypedestroy no ; # immortal

    # # ## ### ##### ######## #############
}

namespace eval ::vc::hyscm::import::cvs::pass {
    namespace export csetdeps
    namespace eval csetdeps {
	namespace import ::vc::hyscm::import::cvs::state
	namespace eval project {
	    namespace import ::vc::hyscm::import::cvs::project::rev
	}
	namespace import ::vc::tools::misc::*
	namespace import ::vc::tools::log
	log register csetdeps
    }
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs::pass::csetdeps 1.0
return
