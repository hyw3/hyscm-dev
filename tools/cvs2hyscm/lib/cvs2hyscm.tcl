## -*- tcl -*-
# Copyright (C) 2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2019 Hyang Language Foundation, Jakarta
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
## Requirements

package require Tcl 8.4                         ; # Required runtime.
package require snit                            ; # OO system

# # ## ### ##### ######## ############# #####################
## Passes. The order in which the various passes are loaded is
##         important. It is the same order in which they will
##         register, and then be run in.

package require vc::hyscm::import::cvs::pass::collar      ; # Coll'ect Ar'chives.
package require vc::hyscm::import::cvs::pass::collrev     ; # Coll'ect Rev'isions.
package require vc::hyscm::import::cvs::pass::collsym     ; # Coll'ate Sym'bols
package require vc::hyscm::import::cvs::pass::filtersym   ; # Filter'  Sym'bols

# Note: cvs2svn's SortRevisionSummaryPass and SortSymbolSummaryPass
#       are not implemented by us. They are irrelevant due to our use
#       of a relational database proper for the persistent state,
#       allowing us to sort the data on the fly as we need it.

package require vc::hyscm::import::cvs::pass::initcsets   ; # Init'ialize C'hange'Sets
package require vc::hyscm::import::cvs::pass::csetdeps    ; # C'hange'Set Dep'endencies
package require vc::hyscm::import::cvs::pass::breakrcycle ; # Break' R'evision Cycle's
package require vc::hyscm::import::cvs::pass::rtopsort    ; # R'evision Top'ological Sort'
package require vc::hyscm::import::cvs::pass::breakscycle ; # Break' S'ymbol Cycle's
package require vc::hyscm::import::cvs::pass::breakacycle ; # Break' A'll Cycle's
package require vc::hyscm::import::cvs::pass::atopsort    ; # A'll Top'ological Sort'
package require vc::hyscm::import::cvs::pass::importfiles ; # Import' Files
package require vc::hyscm::import::cvs::pass::importcsets ; # Import' Changesets
package require vc::hyscm::import::cvs::pass::importfinal ; # Import' Finalization

# # ## ### ##### ######## ############# #####################
## Support for passes etc.

package require vc::hyscm::import::cvs::option ; # Cmd line parsing & database
package require vc::hyscm::import::cvs::pass   ; # Pass management
package require vc::tools::log                  ; # User feedback

# # ## ### ##### ######## ############# #####################
##

snit::type ::vc::hyscm::import::cvs {
    # # ## ### ##### ######## #############
    ## Public API, Methods

    typemethod run {arguments} {
	# Run a series of passes over the cvs repository to extract,
	# filter, and order its historical information. Which passes
	# are actually run is determined through the specified options
	# and their defaults.

	option process $arguments
	pass run

	vc::tools::log write 0 cvs2hyscm Done
	return
    }

    # # ## ### ##### ######## #############
    ## Configuration

    pragma -hasinstances   no ; # singleton
    pragma -hastypeinfo    no ; # no introspection
    pragma -hastypedestroy no ; # immortal

    # # ## ### ##### ######## #############
}

# # ## ### ##### ######## ############# #####################
## Ready

package provide vc::hyscm::import::cvs 1.0
return
