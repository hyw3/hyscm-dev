#!/usr/bin/make
#
# Top-level makefile by default, but HySCM also can be built in a
# directory that is separate from the source tree.  Just change
# the following to adjust to your point.
#
SRCDIR = ./src
OBJDIR = ./bld
BCC = gcc
BCCFLAGS = $(CFLAGS)

# File extension for the final executable hyscm file. When cross-compiling
# to windows, make this ".exe".  Otherwise leave it blank.
#
E =

#TCC = gcc -O6
#TCC = gcc -g -O0 -Wall -fprofile-arcs -ftest-coverage
TCC = gcc -g -Os -Wall

# To use the included miniz library
# HYSCM_ENABLE_MINIZ = 1
# TCC += -DHYSCM_ENABLE_MINIZ

# To add support for HTTPS
TCC += -DHYSCM_ENABLE_SSL

# To enable legacy mv/rm support
TCC += -DHYSCM_ENABLE_LEGACY_MV_RM=1

# We sometimes add the -static option here so that we can build a
# static executable that will run in a chroot jail.
#LIB = -static
TCC += -DHYSCM_DYNAMIC_BUILD=1

TCCFLAGS = $(CFLAGS)

# We don't attempt to use libedit or libreadline in this simplified
# build system (contrast auto.def and Makefile.in) so use the included
# copy of linenoise.  MinGW can't make use of this, but linenoise is
# ifdef'd out elsewhere for that platform.  Note that this is a make
# flag handled in src/main-hyang.mk, not a C preprocessor flag.
USE_LINENOISE := 1

# Extra arguments for linking the finished binary.  HySCM needs
# to link against the Z-Lib compression library unless the miniz
# library in the source tree is being used.  There are no other
# required dependencies.
ZLIB_LIB.0 = -lz
ZLIB_LIB.1 =
ZLIB_LIB.  = $(ZLIB_LIB.0)

# If using zlib:
LIB += $(ZLIB_LIB.$(HYSCM_ENABLE_MINIZ)) $(LDFLAGS)

# If using HTTPS:
LIB += -lcrypto -lssl

# Many platforms put cos() needed by src/piechart.c in libm, rather than
# in libc.  We cannot enable this by default because libm doesn't exist
# everywhere.
#LIB += -lm

# Tcl shell to test HySCM testsuite
#
TCLSH = tclsh
SAWO = sawo

# Keep anything below this line (do not change)
###############################################################################
#
# Automatic platform-specific options.
HOST_OS_CMD = uname -s
HOST_OS = $(HOST_OS_CMD:sh)

LIB.SunOS= -lsocket -lnsl
LIB += $(LIB.$(HOST_OS))

TCC.DragonFly += -DUSE_PREAD
TCC.FreeBSD += -DUSE_PREAD
TCC.NetBSD += -DUSE_PREAD
TCC.OpenBSD += -DUSE_PREAD
TCC += $(TCC.$(HOST_OS))

include $(SRCDIR)/main-hyang.mk
